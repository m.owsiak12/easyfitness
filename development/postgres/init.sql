CREATE USER backend WITH PASSWORD 'backend';
CREATE DATABASE easyfitness;
GRANT ALL PRIVILEGES ON DATABASE easyfitness TO backend;
\connect easyfitness
CREATE EXTENSION "uuid-ossp";
ALTER SCHEMA public OWNER TO backend;