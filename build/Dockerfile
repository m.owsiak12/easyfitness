FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build

ENV NODE_VERSION=18.14.2
RUN apt install -y curl
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

WORKDIR /src

COPY ../src/Easy.Fitness.Domain/. ./Easy.Fitness.Domain/
COPY ../src/Easy.Fitness.Application/. ./Easy.Fitness.Application/
COPY ../src/Easy.Fitness.Infrastructure/. ./Easy.Fitness.Infrastructure/
COPY ../src/Easy.Fitness.Web/. ./Easy.Fitness.Web/

WORKDIR /src/Easy.Fitness.Web
RUN dotnet restore
RUN dotnet publish -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS runtime
WORKDIR /app
COPY --from=build /src/Easy.Fitness.Web/out ./

ENV DbConnection:ConnectionString="User ID=backend;Password=backend;Host=postgres;Port=5432;Database=easyfitness;Keepalive=600"

ENTRYPOINT [ "dotnet", "Easy.Fitness.Web.dll" ]