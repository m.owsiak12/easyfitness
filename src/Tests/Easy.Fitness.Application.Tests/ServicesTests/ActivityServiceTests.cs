﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Services;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Application.Tests.ServicesTests
{
    [TestFixture]
    public class ActivityServiceTests
    {
        private Mock<IActivityRepository> _activityRepositoryMock;
        private ActivityService _activityService;
        private const int PAGE_SIZE = 7;

        [SetUp]
        public void Setup()
        {
            _activityRepositoryMock = new Mock<IActivityRepository>();
            _activityService = new ActivityService(_activityRepositoryMock.Object);
        }

        [Test]
        public async Task Should_save_new_activity_and_return()
        {
            // Arrange
            ActivityDto dto = new()
            {
                Date = "23.03.2024",
                Type = "Other",
                Name = "Other",
                Calories = 500,
                Duration = "2:30:00"
            };
            Activity entity = new(dto.Date, dto.Type, dto.Name, dto.Calories, dto.Duration);
            _activityRepositoryMock.Setup(x => x.SaveNewActivityAsync(It.IsAny<Activity>(), CancellationToken.None)).ReturnsAsync(entity);

            // Act
            ActivityDto result = await _activityService.SaveNewActivityAsync(dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(entity.Date));
            Assert.That(result.Type, Is.EqualTo(entity.Type));
            Assert.That(result.Name, Is.EqualTo(entity.Name));
            Assert.That(result.Calories, Is.EqualTo(entity.Calories));
            Assert.That(result.Duration, Is.EqualTo(entity.Duration));
        }

        [Test]
        public async Task Should_get_page_of_activities()
        {
            // Arrange
            GetPageCriteria criteria = new()
            {
                Page = 1,
                SortColumn = "Date",
                IsDescending = true,
                SearchType = "All",
                SearchDate = "24.03.2024"
            };
            int totalCount = 5;
            _activityRepositoryMock.Setup(x => x.GetTotalCountAsync(CancellationToken.None)).ReturnsAsync(totalCount);
            _activityRepositoryMock.Setup(x => x.GetActivitiesAsync(
                criteria.Page, criteria.SortColumn, criteria.IsDescending, criteria.SearchType, criteria.SearchDate, CancellationToken.None))
                .ReturnsAsync(new List<Activity>() { new Activity("24.03.2024", "Swimming", "swim", 654, "1:45:00") });

            // Act
            PageDto<ActivityDto> result = await _activityService.GetActivityPageAsync(criteria, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result.Items);
            Assert.That(result.Items.Count, Is.EqualTo(1));
            Assert.That(result.Page, Is.EqualTo(criteria.Page));
            Assert.That(result.PageSize, Is.EqualTo(PAGE_SIZE));
        }

        [Test]
        public async Task Should_update_activity()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            ActivityDto dto = new()
            {
                Date = "24.03.2024",
                Type = "Swimming",
                Name = "swim",
                Calories = 640,
                Duration = "1:45:00",
                Id = id
            };
            _activityRepositoryMock.Setup(x => x.UpdateActivityAsync(id, It.IsAny<Activity>(), CancellationToken.None))
                .ReturnsAsync(new Activity("24.03.2024", "Swimming", "swim", 640, "1:45:00"));

            // Act
            ActivityDto result = await _activityService.UpdateActivityAsync(id, dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(dto.Date));
            Assert.That(result.Type, Is.EqualTo(dto.Type));
            Assert.That(result.Name, Is.EqualTo(dto.Name));
            Assert.That(result.Calories, Is.EqualTo(dto.Calories));
            Assert.That(result.Duration, Is.EqualTo(dto.Duration));
        }

        [Test]
        public async Task Should_delete_activity()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            _activityRepositoryMock.Setup(x => x.DeleteActivityAsync(id, CancellationToken.None)).Returns(Task.CompletedTask);

            // Act
            await _activityService.DeleteActivityAsync(id, CancellationToken.None);

            // Assert
            _activityRepositoryMock.Verify(x => x.DeleteActivityAsync(It.IsAny<Guid>(), CancellationToken.None), Times.Once());
        }
    }
}