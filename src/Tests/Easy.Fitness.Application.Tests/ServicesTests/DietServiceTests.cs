﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos.Diet;
using Easy.Fitness.Application.Services;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Application.Tests.ServicesTests
{
    [TestFixture]
    public class DietServiceTests
    {
        private Mock<IDietRepository> _dietRepositoryMock;
        private Mock<IFoodDataProvider> _foodProviderMock;
        private DietService _dietService;

        [SetUp]
        public void Setup()
        {
            _dietRepositoryMock = new Mock<IDietRepository>();
            _foodProviderMock = new Mock<IFoodDataProvider>();
            _dietService = new DietService(_dietRepositoryMock.Object, _foodProviderMock.Object);
        }

        [Test]
        public async Task Should_add_new_food_to_diet()
        {
            // Arrange
            AddNewFoodDto dto = new()
            {
                Date = "2024-04-06",
                Name = "Chicken",
                Type = "Dinner",
                Weight = 100
            };
            FoodDetails foodDetails = new("Chicken", 250, 6, 20, 35, 100);
            Food entity = new("Chicken", 250, 6, 20, 35, 100, "Dinner");
            _foodProviderMock.Setup(x => x.GetFoodDetails(It.IsAny<string>())).Returns(foodDetails);
            _dietRepositoryMock.Setup(x => x.AddNewFoodToDietAsync(It.IsAny<Food>(), dto.Date, CancellationToken.None))
                .ReturnsAsync(entity);

            // Act
            FoodDto result = await _dietService.AddNewFoodToDietAsync(dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Name, Is.EqualTo(entity.Name));
            Assert.That(result.Calories, Is.EqualTo(entity.Calories));
            Assert.That(result.Fat, Is.EqualTo(entity.Fat));
            Assert.That(result.Carbs, Is.EqualTo(entity.Carbs));
            Assert.That(result.Protein, Is.EqualTo(entity.Protein));
            Assert.That(result.Type, Is.EqualTo(entity.Type));
            Assert.That(result.Weight, Is.EqualTo(entity.Weight));
        }

        [Test]
        public async Task Should_delete_food()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            string date = "2024-04-06";
            _dietRepositoryMock.Setup(x => x.DeleteFoodAsync(id, date, CancellationToken.None)).Returns(Task.CompletedTask);

            // Act
            await _dietService.DeleteFoodAsync(id, date, CancellationToken.None);

            // Assert
            _dietRepositoryMock.Verify(x => x.DeleteFoodAsync(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_get_diet_by_date()
        {
            // Arrange
            string date = "2024-04-06";
            Diet diet = new(date, 3000, 50, 180, 250);
            _dietRepositoryMock.Setup(x => x.GetDietByDateAsync(date, CancellationToken.None)).ReturnsAsync(diet);

            // Act
            DietDto result = await _dietService.GetDietByDateAsync(date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Calories, Is.EqualTo(diet.Calories));
            Assert.That(result.Fat, Is.EqualTo(diet.Fat));
            Assert.That(result.Carbs, Is.EqualTo(diet.Carbs));
            Assert.That(result.Protein, Is.EqualTo(diet.Protein));
        }

        [Test]
        public async Task Should_get_diet_properties_by_date()
        {
            // Arrange
            string date = "2024-04-06";
            DietProperties dietProperties = new(date, 3500, 65, 200, 280);
            _dietRepositoryMock.Setup(x => x.GetDietParametersAsync(date, CancellationToken.None)).ReturnsAsync(dietProperties);

            // Act
            DietPropertiesDto result = await _dietService.GetDietPropertiesAsync(date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(dietProperties.Date));
            Assert.That(result.Calories, Is.EqualTo(dietProperties.Calories));
            Assert.That(result.Fat, Is.EqualTo(dietProperties.Fat));
            Assert.That(result.Carbs, Is.EqualTo(dietProperties.Carbs));
            Assert.That(result.Protein, Is.EqualTo(dietProperties.Protein));
        }

        [Test]
        public async Task Should_get_diet_summary_by_date()
        {
            // Arrange
            string date = "2024-04-06";
            DietSummary dietSummary = new()
            {
                CurrentCalories = 1500,
                MaxCalories = 3500,
                CurrentCarbs = 70,
                MaxCarbs = 180,
                CurrentFat = 20,
                MaxFat = 55,
                CurrentProtein = 120,
                MaxProtein = 220
            };
            _dietRepositoryMock.Setup(x => x.GetDietSummaryByDateAsync(date, CancellationToken.None)).ReturnsAsync(dietSummary);

            // Act
            DietSummary result = await _dietService.GetDietSummaryByDateAsync(date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.CurrentCalories, Is.EqualTo(dietSummary.CurrentCalories));
            Assert.That(result.MaxCalories, Is.EqualTo(dietSummary.MaxCalories));
            Assert.That(result.CurrentCarbs, Is.EqualTo(dietSummary.CurrentCarbs));
            Assert.That(result.MaxCarbs, Is.EqualTo(dietSummary.MaxCarbs));
            Assert.That(result.CurrentFat, Is.EqualTo(dietSummary.CurrentFat));
            Assert.That(result.MaxProtein, Is.EqualTo(dietSummary.MaxProtein));
            Assert.That(result.CurrentProtein, Is.EqualTo(dietSummary.CurrentProtein));
        }

        [Test]
        public void Should_get_food_name_list()
        {
            // Arrange
            string foodName = "chic";
            _foodProviderMock.Setup(x => x.GetFoodNameHints(foodName)).Returns(new List<string>() { "chicken" });

            // Act
            List<string> result = _dietService.GetFoodNameList(foodName);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Count, Is.EqualTo(1));
            Assert.That(result[0], Is.EqualTo("chicken"));
        }

        [Test]
        public async Task Should_save_diet_properties()
        {
            // Arrange
            DietPropertiesDto dto = new()
            {
                Date = "2024-04-06",
                Calories = 3000,
                Carbs = 180,
                Fat = 50,
                Protein = 220
            };
            DietProperties entity = new(dto.Date, dto.Calories, dto.Fat, dto.Carbs, dto.Protein);
            _dietRepositoryMock.Setup(x => x.SaveDietParametersAsync(It.IsAny<DietProperties>(), CancellationToken.None))
                .ReturnsAsync(entity);

            // Act
            DietPropertiesDto result = await _dietService.SaveDietPropertiesAsync(dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(entity.Date));
            Assert.That(result.Calories, Is.EqualTo(entity.Calories));
            Assert.That(result.Fat, Is.EqualTo(entity.Fat));
            Assert.That(result.Carbs, Is.EqualTo(entity.Carbs));
            Assert.That(result.Protein, Is.EqualTo(entity.Protein));
        }

        [Test]
        public async Task Should_update_food()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            UpdateFoodDto dto = new()
            {
                Name = "Chicken",
                Weight = 100,
                Type = "Dinner",
                Date = "2024-04-06"
            };
            FoodDetails foodDetails = new("Chicken", 250, 6, 20, 35, 100);
            Food entity = new("Chicken", 250, 6, 20, 35, 100, "Dinner");
            _foodProviderMock.Setup(x => x.GetFoodDetails(It.IsAny<string>())).Returns(foodDetails);
            _dietRepositoryMock.Setup(x => x.UpdateFoodAsync(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<Food>(), CancellationToken.None))
                .ReturnsAsync(entity);

            // Act
            FoodDto result = await _dietService.UpdateFoodAsync(id, dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Name, Is.EqualTo(entity.Name));
            Assert.That(result.Calories, Is.EqualTo(entity.Calories));
            Assert.That(result.Fat, Is.EqualTo(entity.Fat));
            Assert.That(result.Carbs, Is.EqualTo(entity.Carbs));
            Assert.That(result.Protein, Is.EqualTo(entity.Protein));
            Assert.That(result.Type, Is.EqualTo(entity.Type));
            Assert.That(result.Weight, Is.EqualTo(entity.Weight));
        }
    }
}