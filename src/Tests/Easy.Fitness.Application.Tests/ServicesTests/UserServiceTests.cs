﻿using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.User;
using Easy.Fitness.Application.Services;
using Easy.Fitness.Domain;
using Easy.Fitness.Domain.Ids;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Moq;
using NUnit.Framework;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Easy.Fitness.Application.Tests.ServicesTests
{
    [TestFixture]
    public class UserServiceTests
    {
        private Mock<IUserRepository> _userRepositoryMock;
        private Mock<IUserTokenProvider> _tokenProviderMock;
        private Mock<IUserContext> _userContextMock;
        private Mock<IFileService> _fileServiceMock;
        private UserService _userService;

        private const string EXAMPLE_ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJPbmxpbmUgSldUIEJ1aWxkZXIiLCJpYXQiOjE3MTA2MjI4ODYsImV4cCI6MTc0MjE1ODg4NiwiYXVkIjoid3d3LmV4YW1wbGUuY29tIiwic3ViIjoianJvY2tldEBleGFtcGxlLmNvbSIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjpbIk1hbmFnZXIiLCJQcm9qZWN0IEFkbWluaXN0cmF0b3IiXX0.sJrIPyBGrkjgryTNP7nxct_zE3CnhJauifr7gl1dBAs";

        [SetUp]
        public void Setup()
        {
            _userRepositoryMock = new Mock<IUserRepository>();
            _tokenProviderMock = new Mock<IUserTokenProvider>();
            _userContextMock = new Mock<IUserContext>();
            _fileServiceMock = new Mock<IFileService>();
            _userService = new UserService(_userRepositoryMock.Object, _tokenProviderMock.Object, _userContextMock.Object, _fileServiceMock.Object);
        }

        [Test]
        public async Task Should_create_new_user()
        {
            // Arrange
            CreateUserDto dto = new()
            {
                Email = "test@test.pl",
                Password = "!2Qwerty"
            };
            _userRepositoryMock.Setup(x => x.AddUserAsync(It.IsAny<User>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User(Guid.NewGuid(), dto.Email, dto.Password));

            // Act
            UserDto result = await _userService.CreateNewUserAsync(dto, It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Email, Is.EqualTo(dto.Email));
            Assert.That(result.Password, Is.Not.EqualTo(dto.Password));
            _userRepositoryMock.Verify(x => x.AddUserAsync(It.IsAny<User>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_authenticate_user()
        {
            // Arrange
            CreateUserDto dto = new()
            {
                Email = "test@test.pl",
                Password = "!2Qwerty"
            };
            _userRepositoryMock.Setup(x => x.GetUserByEmailAsync(dto.Email, It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User(Guid.NewGuid(), dto.Email, dto.Password));
            _tokenProviderMock.Setup(x => x.GetAccessToken()).Returns(EXAMPLE_ACCESS_TOKEN);

            // Act
            string result = await _userService.AuthenticateUserAsync(dto, It.IsAny<CancellationToken>());

            // Assert
            Assert.That(result, Is.EqualTo(EXAMPLE_ACCESS_TOKEN));
            _userRepositoryMock.Verify(x => x.GetUserByEmailAsync(It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once());
            _tokenProviderMock.Verify(x => x.GetAccessToken(), Times.Once());
        }

        [Test]
        public async Task Should_update_user()
        {
            // Arrange
            UserInfoDto dto = new()
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                PhoneNumber = "+48123123123",
                BirthDate = "31.07.2001"
            };
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.UpdateUserAsync(It.IsAny<User>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new User("test@test.pl", "!2Qwerty", dto.FirstName, dto.LastName, dto.PhoneNumber, dto.BirthDate));

            // Act
            UserInfoDto result = await _userService.UpdateUserAsync(dto, It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.FirstName, Is.EqualTo(dto.FirstName));
            Assert.That(result.LastName, Is.EqualTo(dto.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(dto.PhoneNumber));
            _userContextMock.Verify(x => x.CurrentUserId, Times.Once());
            _userRepositoryMock.Verify(x => x.UpdateUserAsync(It.IsAny<User>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_get_user_info()
        {
            // Arrange
            UserInfoDto dto = new()
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                PhoneNumber = "+48123123123",
                BirthDate = "31.07.2001"
            };
            User user = new("test@test.pl", "!2Qwerty", dto.FirstName, dto.LastName, dto.PhoneNumber, dto.BirthDate);
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.GetUserByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(user);

            // Act
            UserInfoDto result = await _userService.GetUserInfoAsync(It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.FirstName, Is.EqualTo(dto.FirstName));
            Assert.That(result.LastName, Is.EqualTo(dto.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(dto.PhoneNumber));
            _userContextMock.Verify(x => x.CurrentUserId, Times.Once());
            _userRepositoryMock.Verify(x => x.GetUserByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_update_user_parameters()
        {
            // Arrange
            UserParametersDto dto = new()
            {
                Weight = 80,
                Height = 180
            };
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.SaveUserParametersAsync(It.IsAny<Guid>(), It.IsAny<UserParameters>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(new UserParameters(dto.Weight, dto.Height));

            // Act
            UserParametersDto result = await _userService.UpdateUserParametersAsync(dto, It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Weight, Is.EqualTo(dto.Weight));
            Assert.That(result.Height, Is.EqualTo(dto.Height));
            _userContextMock.Verify(x => x.CurrentUserId, Times.Exactly(2));
            _userRepositoryMock.Verify(x => x.SaveUserParametersAsync(It.IsAny<Guid>(), It.IsAny<UserParameters>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_get_latest_user_parameters()
        {
            // Arrange
            UserParameters userParams = new(80, 180);
            UserParametersDto dto = new()
            {
                Height = userParams.Height,
                Weight = userParams.Weight
            };
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.GetLatestUserParametersByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(userParams);

            // Act
            UserParametersDto result = await _userService.GetLatestUserParametersAsync(It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Weight, Is.EqualTo(dto.Weight));
            Assert.That(result.Height, Is.EqualTo(dto.Height));
            _userContextMock.Verify(x => x.CurrentUserId, Times.Once());
            _userRepositoryMock.Verify(x => x.GetLatestUserParametersByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_get_user_personal_info()
        {
            // Arrange
            UserAccountDto dto = new()
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                BirthDate = "31.07.2001"
            };
            User user = new("test@test.pl", "!2Qwerty", dto.FirstName, dto.LastName, "+48123123123", dto.BirthDate);
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.GetUserByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(user);

            // Act
            UserAccountDto result = await _userService.GetUserPersonalInfoAsync(It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.FirstName, Is.EqualTo(dto.FirstName));
            Assert.That(result.LastName, Is.EqualTo(dto.LastName));
            _userContextMock.Verify(x => x.CurrentUserId, Times.Once());
            _userRepositoryMock.Verify(x => x.GetUserByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()), Times.Once());
        }

        [Test]
        public async Task Should_change_password()
        {
            // Arrange
            ChangePasswordDto dto = new()
            {
                CurrentPassword = "!2Qwerty",
                NewPassword = "NewPassword123"
            };
            User user = new("test@test.pl", dto.CurrentPassword);
            _userRepositoryMock.Setup(x => x.GetUserByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(user);
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.UpdateUserPasswordAsync(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<CancellationToken>()))
                .Callback((Guid id, string newPassword, CancellationToken cancellationToken) =>
                {

                }).Returns(Task.CompletedTask);

            // Act
            await _userService.ChangeUserPasswordAsync(dto, It.IsAny<CancellationToken>());

            // Assert
            _userRepositoryMock.Verify(x => x.GetUserByIdAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()), Times.Once());
            _userRepositoryMock.Verify(x => x.UpdateUserPasswordAsync(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<CancellationToken>()), Times.Once());
            _userContextMock.Verify(x => x.CurrentUserId, Times.Exactly(2));
        }

        [Test]
        public async Task Should_get_user_summary()
        {
            // Arrange
            UserSummary summary = new()
            {
                Calories = 500,
                Trainings = 1
            };
            _userContextMock.Setup(x => x.CurrentUserId)
                .Returns(UserId.FromGuid(Guid.NewGuid()));
            _userRepositoryMock.Setup(x => x.GetUserSummaryAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()))
                .ReturnsAsync(summary);

            // Act
            UserSummary result = await _userService.GetUserSummaryAsync(It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(summary.Trainings, result.Trainings);
            Assert.AreEqual(summary.Calories, result.Calories);
            _userRepositoryMock.Verify(x => x.GetUserSummaryAsync(It.IsAny<Guid>(), It.IsAny<CancellationToken>()), Times.Once());
            _userContextMock.Verify(x => x.CurrentUserId, Times.Once());
        }
    }
}