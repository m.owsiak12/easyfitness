﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Dtos.Schedule;
using Easy.Fitness.Application.Services;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Application.Tests.ServicesTests
{
    [TestFixture]
    public class ScheduleServiceTests
    {
        private Mock<IScheduleRepository> _scheduleRepositoryMock;
        private ScheduleService _scheduleService;
        private const int PAGE_SIZE = 7;

        [SetUp]
        public void Setup()
        {
            _scheduleRepositoryMock = new Mock<IScheduleRepository>();
            _scheduleService = new ScheduleService(_scheduleRepositoryMock.Object);
        }

        [Test]
        public async Task Should_save_new_schedule()
        {
            // Arrange
            ScheduleDto dto = new()
            {
                Date = "24.03.2024",
                Type = "Other",
                Note = "test"
            };
            PlannedActivity entity = new(dto.Date, dto.Type, dto.Note);
            _scheduleRepositoryMock.Setup(x => x.SaveNewScheduleAsync(It.IsAny<PlannedActivity>(), CancellationToken.None)).ReturnsAsync(entity);

            // Act
            ScheduleDto result = await _scheduleService.SaveNewScheduleAsync(dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(entity.Date));
            Assert.That(result.Type, Is.EqualTo(entity.Type));
            Assert.That(result.Note, Is.EqualTo(entity.Note));
        }

        [Test]
        public async Task Should_get_page_of_planned_activities()
        {
            // Arrange
            GetPageCriteria criteria = new()
            {
                Page = 1,
                SortColumn = "Date",
                IsDescending = true,
                SearchType = "All",
                SearchDate = "24.03.2024"
            };
            int totalCount = 5;
            _scheduleRepositoryMock.Setup(x => x.GetTotalCountAsync(CancellationToken.None)).ReturnsAsync(totalCount);
            _scheduleRepositoryMock.Setup(x => x.GetSchedulesAsync(
                criteria.Page, criteria.SortColumn, criteria.IsDescending, criteria.SearchType, criteria.SearchDate, CancellationToken.None))
                .ReturnsAsync(new List<PlannedActivity>() { new PlannedActivity("24.03.2024", "Other", "test") });

            // Act
            PageDto<ScheduleDto> result = await _scheduleService.GetSchedulePageAsync(criteria, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result.Items);
            Assert.That(result.Items.Count, Is.EqualTo(1));
            Assert.That(result.Page, Is.EqualTo(criteria.Page));
            Assert.That(result.PageSize, Is.EqualTo(PAGE_SIZE));
        }

        [Test]
        public async Task Should_update_schedule()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            ScheduleDto dto = new()
            {
                Date = "24.03.2024",
                Type = "Other",
                Note = "test",
                Id = id
            };
            _scheduleRepositoryMock.Setup(x => x.UpdateScheduleAsync(id, It.IsAny<PlannedActivity>(), CancellationToken.None))
                .ReturnsAsync(new PlannedActivity(dto.Date, dto.Type, dto.Note));

            // Act
            ScheduleDto result = await _scheduleService.UpdateScheduleAsync(id, dto, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(dto.Date));
            Assert.That(result.Type, Is.EqualTo(dto.Type));
            Assert.That(result.Note, Is.EqualTo(dto.Note));
        }

        [Test]
        public async Task Should_delete_schedule()
        {
            // Arrange
            Guid id = Guid.NewGuid();
            _scheduleRepositoryMock.Setup(x => x.DeleteScheduleAsync(id, CancellationToken.None)).Returns(Task.CompletedTask);

            // Act
            await _scheduleService.DeleteScheduleAsync(id, CancellationToken.None);

            // Assert
            _scheduleRepositoryMock.Verify(x => x.DeleteScheduleAsync(It.IsAny<Guid>(), CancellationToken.None), Times.Once());
        }
    }
}