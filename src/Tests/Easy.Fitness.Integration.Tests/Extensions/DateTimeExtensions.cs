﻿using System;

namespace Easy.Fitness.Integration.Tests.Extensions
{
    internal static class DateTimeExtensions
    {
        public static DateTime TruncateToMiliseconds(this DateTime dateTime)
        {
            return dateTime.Truncate(TimeSpan.FromMilliseconds(1));
        }

        public static DateTime TruncateToSeconds(this DateTime dateTime)
        {
            return dateTime.Truncate(TimeSpan.FromSeconds(1));
        }

        public static DateTime TruncateToMinutes(this DateTime dateTime)
        {
            return dateTime.Truncate(TimeSpan.FromMinutes(1));
        }

        private static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
        {
            if (timeSpan == TimeSpan.Zero)
            {
                return dateTime;
            }
            return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
        }
    }
}