﻿using System;

namespace Easy.Fitness.Integration.Tests.Extensions
{
    public static class UrlExtensions
    {
        private const char SLASH_MARK = '/';

        public static Uri ToUriWithSufix(this Uri uri, string sufix)
        {
            string baseAddress = uri.AbsoluteUri;
            if (baseAddress.EndsWith(SLASH_MARK.ToString()))
            {
                baseAddress = baseAddress.TrimEnd(SLASH_MARK);
            }
            if (sufix.StartsWith(SLASH_MARK.ToString()))
            {
                sufix = sufix.TrimStart(SLASH_MARK);
            }
            return new Uri($"{baseAddress}/{sufix}");
        }
    }
}