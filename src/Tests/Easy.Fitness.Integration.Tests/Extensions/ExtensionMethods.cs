﻿using System;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Http;

namespace Easy.Fitness.Integration.Tests.Extensions
{
    public static class ExtensionMethods
    {
        //TODO: Fix, bo nie dziala coś, wychodzi pusty queryString pomimo ze properties sie dodają
        public static QueryString ToQueryString(this object obj)
        {
            var props = obj.GetType().GetProperties()
                .Where(x => x.CanRead)
                .Select(x => new { Value = x.GetValue(obj, null), Property = x });

            QueryString queryString = new();
            foreach (var p in props)
            {
                queryString.Add(p.Property.Name, GetValue(p.Property, p.Value));
            }
            return queryString;
        }

        private static string GetValue(PropertyInfo p, object value)
        {
            if (p.PropertyType == typeof(DateTime?))
            {
                DateTime? dateTime = (DateTime?)value;
                return DateTimeValue(dateTime.Value);
            }
            if (p.PropertyType == typeof(DateTime))
            {
                DateTime dateTime = (DateTime)value;
                return DateTimeValue(dateTime);
            }
            return value.ToString();
        }

        private static string DateTimeValue(DateTime dateTime)
        {
            var date = new DateTimeOffset(dateTime);
            return date.ToUnixTimeMilliseconds().ToString();
        }
    }
}