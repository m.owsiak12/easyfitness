﻿using System;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Web;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Microsoft.Extensions.Hosting;
using Serilog;

namespace Easy.Fitness.Integration.Tests
{
    public class Server : IDisposable
    {
        public HttpClient Client { get; private set; }
        public IServiceProvider ServiceContainer { get; private set; }
        public TestServer TestServer { get; private set; }
        private bool _disposed;

        public async Task StartAsync()
        {
            InitializeLogger();
            var host = await Host.CreateDefaultBuilder()
                .ConfigureWebHostDefaults(builder =>
                {
                    builder
                        .UseContentRoot(Directory.GetCurrentDirectory())
                        .UseStartup<Startup>()
                        .UseTestServer();
                })
                .UseEnvironment("Development")
                .StartAsync();
            var server = host.GetTestServer();
            Client = host.GetTestClient();
            TestServer = server;
            ServiceContainer = host.Services;
        }

        private static void InitializeLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom
                .Configuration(ConfigurationHelper.CreateRoot())
                .CreateLogger();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }

            if (disposing)
            {
                Client.Dispose();
                TestServer.Dispose();
            }
            _disposed = true;
        }
    }
}