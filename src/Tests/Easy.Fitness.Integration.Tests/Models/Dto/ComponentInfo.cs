﻿using Newtonsoft.Json;

namespace Easy.Fitness.Integration.Tests.Models.Dto
{
    public class ComponentInfo
    {
        [JsonProperty("version")]
        public string Version { get; set; }

        [JsonProperty("supportedApiVersions")]
        public string[] SupportedApiVersions { get; set; }
    }
}