﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Converters;
using Easy.Fitness.Infrastructure.Exceptions;
using Easy.Fitness.Integration.Tests.Extensions;
using Newtonsoft.Json;
using Serilog;

namespace Easy.Fitness.Integration.Tests.WebClients
{
    public class BaseWebClient
    {
        private const string BEARER_AUTH_TYPE = "Bearer";
        private readonly IAuthorizationContext _authorizationContext;
        private readonly JsonSerializerSettings _serializerSettings;

        public HttpStatusCode LastStatusCode { get; private set; }
        protected HttpClient HttpClient { get; }

        private Uri BaseAddress
        {
            get { return HttpClient.BaseAddress; }
        }

        protected BaseWebClient(HttpClient httpClient,
            IAuthorizationContext authorizationContext)
        {
            _authorizationContext = authorizationContext ?? throw new ArgumentNullException(nameof(authorizationContext));
            HttpClient = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _serializerSettings = new JsonSerializerSettings();
            _serializerSettings.RegisterAllConverters();
        }

        protected Task GetAsync(string requestUrl)
        {
            return SendAsync<string>(HttpMethod.Get, requestUrl);
        }

        protected Task<TResult> GetAsync<TResult>(string url, object model)
        {
            return GetAsync<TResult>($"{url}{model.ToQueryString()}");
        }

        protected Task<TResponse> GetAsync<TResponse>(string requestUrl)
        {
            return SendAsync<TResponse>(HttpMethod.Get, requestUrl);
        }

        protected async Task<TResponse> PostAsync<TResponse, TMessage>(string requestUrl,
            TMessage message) where TMessage : class
        {
            return await SendAsync<TResponse, TMessage>(HttpMethod.Post, requestUrl, message);
        }

        protected async Task<TResponse> PostAsync<TResponse>(string requestUrl)
        {
            return await SendAsync<TResponse>(HttpMethod.Post, requestUrl);
        }

        protected async Task<TResponse> PutAsync<TResponse>(string requestUrl)
        {
            return await SendAsync<TResponse>(HttpMethod.Put, requestUrl);
        }

        protected async Task<TResponse> PutAsync<TResponse, TMessage>(string requestUrl, TMessage message)
            where TMessage : class
        {
            return await SendAsync<TResponse, TMessage>(HttpMethod.Put, requestUrl, message);
        }

        protected async Task<TResponse> DeleteAsync<TResponse>(string requestUrl)
        {
            return await SendAsync<TResponse>(HttpMethod.Delete, requestUrl);
        }

        private async Task<TResponse> SendAsync<TResponse>(HttpMethod httpMethod, string requestUrl)
        {
            Log.Information($"{httpMethod.Method} address: {requestUrl}", requestUrl);
            HttpRequestMessage request = await CreateRequestAsync(requestUrl, httpMethod);
            return await SendAsync<TResponse>(request);
        }

        private async Task<TResponse> SendAsync<TResponse, TMessage>(HttpMethod httpMethod, string requestUrl,
            TMessage message) where TMessage : class
        {
            Log.Information($"{httpMethod.Method} address: {requestUrl}", requestUrl);
            string serializedMessage = SerializeItem(message);
            HttpContent content = new StringContent(serializedMessage, Encoding.UTF8, "application/json");
            HttpRequestMessage request = await CreateRequestAsync(requestUrl, httpMethod, content);
            return await SendAsync<TResponse>(request);
        }

        private async Task<T> SendAsync<T>(HttpRequestMessage request)
        {
            Log.Information("SendAsync | {RequestUri}", request.RequestUri);
            using HttpResponseMessage response = await HttpClient.SendAsync(request);
            string message = await ReadAsStringAsync(response);
            if (response.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<T>(message, _serializerSettings);
            }
            else
            {
                Log.Error("SendAsync Error,  {url}, {responseStatusCode}, {responseReasonPhrase} {message}",
                    request.RequestUri.AbsoluteUri, response.StatusCode, response.ReasonPhrase, message);
                throw new ResponseException(response.StatusCode, message);
            }
        }

        private async Task<HttpRequestMessage> CreateRequestAsync(
            string address,
            HttpMethod method,
            HttpContent content = null)
        {
            HttpRequestMessage requestMessage = new()
            {
                Method = method,
                RequestUri = BaseAddress.ToUriWithSufix(address)
            };
            await SetToken(requestMessage);
            Log.Information("Create request message for url: {requestedUrl}", requestMessage.RequestUri);
            if (content != null)
            {
                requestMessage.Content = content;
            }
            return requestMessage;
        }

        private async Task SetToken(HttpRequestMessage requestMessage)
        {
            string accessToken = await _authorizationContext.GetAccessTokenAsync();
            requestMessage.Headers.Authorization =
                new AuthenticationHeaderValue(BEARER_AUTH_TYPE, accessToken);
        }

        private async Task<string> ReadAsStringAsync(HttpResponseMessage message)
        {
            using (message)
            {
                LastStatusCode = message.StatusCode;
                return await message.Content.ReadAsStringAsync();
            }
        }

        private string SerializeItem<TItem>(TItem item) where TItem : class
        {
            try
            {
                return JsonConvert.SerializeObject(item, _serializerSettings);
            }
            catch (Exception ex)
            {
                Log.Error("SerializeItem Error, {Exception}", ex);
                throw;
            }
        }
    }
}