﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Analysis.Activity;
using Easy.Fitness.Application.Dtos.Analysis.Diet;
using Easy.Fitness.Application.Dtos.Analysis.Weight;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Dtos.Diet;
using Easy.Fitness.Application.Dtos.Schedule;
using Easy.Fitness.Application.Dtos.User;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Authorization;
using Easy.Fitness.Integration.Tests.Models.Dto;
using Easy.Fitness.Web.HealthChecks;
using Microsoft.AspNetCore.Mvc;

namespace Easy.Fitness.Integration.Tests.WebClients
{
    public class ApiWebClient : BaseWebClient, IDisposable
    {
        private bool _disposed;

        public ApiWebClient(HttpClient httpClient, IAuthorizationContext authorizationContext) : base(httpClient, authorizationContext)
        {
        }

        public async Task<ComponentInfo> GetInfo()
        {
            const string url = "api/info";
            return await GetAsync<ComponentInfo>(url);
        }

        public Task<HealthReportDto> GetHealth()
        {
            const string url = "healthz";
            return GetAsync<HealthReportDto>(url);
        }

        public Task<HealthReportDto> GetReady()
        {
            const string url = "readyz";
            return GetAsync<HealthReportDto>(url);
        }

        public Task<UserInfoDto> GetUserInfo()
        {
            const string url = "api/v1/user";
            return GetAsync<UserInfoDto>(url);
        }

        public Task<UserInfoDto> UpdateUser(UserInfoDto dto)
        {
            const string url = "api/v1/user";
            return PutAsync<UserInfoDto, UserInfoDto>(url, dto);
        }

        public Task ChangeUserPassword(ChangePasswordDto dto)
        {
            const string url = "api/v1/user/password";
            return PutAsync<StatusCodeResult, ChangePasswordDto>(url, dto);
        }

        public Task<UserParametersDto> GetUserParameters()
        {
            const string url = "api/v1/user/parameters";
            return GetAsync<UserParametersDto>(url);
        }

        public Task<UserAccountDto> GetUserPersonalInfo()
        {
            const string url = "api/v1/user/account";
            return GetAsync<UserAccountDto>(url);
        }

        public Task<UserSummary> GetUserSummary()
        {
            const string url = "api/v1/user/summary";
            return GetAsync<UserSummary>(url);
        }

        public Task<UserParametersDto> SaveUserParameters(UserParametersDto dto)
        {
            const string url = "api/v1/user/parameters";
            return PostAsync<UserParametersDto, UserParametersDto>(url, dto);
        }

        public Task<AccessTokenDto> AuthenticateUser(CreateUserDto dto)
        {
            const string url = "api/v1/login";
            return PostAsync<AccessTokenDto, CreateUserDto>(url, dto);
        }

        public Task<UserDto> CreateUser(CreateUserDto dto)
        {
            const string url = "api/v1/register";
            return PostAsync<UserDto, CreateUserDto>(url, dto);
        }

        public Task<ActivityDto> SaveActivity(ActivityDto dto)
        {
            const string url = "api/v1/activity";
            return PostAsync<ActivityDto, ActivityDto>(url, dto);
        }

        public Task<PageDto<ActivityDto>> GetActivities(GetPageCriteria criteria)
        {
            string url = "api/v1/activity";
            string queryString = $"?count={criteria.Count}&isDescending={criteria.IsDescending}&page={criteria.Page}" +
                $"&sortColumn={criteria.SortColumn}&searchType={criteria.SearchType}&searchDate={criteria.SearchDate}";
            url += queryString;
            return GetAsync<PageDto<ActivityDto>>(url);
        }

        public Task DeleteActivity(Guid activityId)
        {
            string url = $"api/v1/activity/{activityId}";
            return DeleteAsync<StatusCodeResult>(url);
        }

        public Task<ActivityDto> UpdateActivity(Guid activityId, ActivityDto dto)
        {
            string url = $"api/v1/activity/{activityId}";
            return PutAsync<ActivityDto, ActivityDto>(url, dto);
        }

        public Task<ScheduleDto> SaveSchedule(ScheduleDto dto)
        {
            const string url = "api/v1/schedule";
            return PostAsync<ScheduleDto, ScheduleDto>(url, dto);
        }

        public Task<PageDto<ScheduleDto>> GetSchedule(GetPageCriteria criteria)
        {
            string url = "api/v1/schedule";
            string queryString = $"?count={criteria.Count}&isDescending={criteria.IsDescending}&page={criteria.Page}" +
                $"&sortColumn={criteria.SortColumn}&searchType={criteria.SearchType}&searchDate={criteria.SearchDate}";
            url += queryString;
            return GetAsync<PageDto<ScheduleDto>>(url);
        }

        public Task DeleteSchedule(Guid scheduleId)
        {
            string url = $"api/v1/schedule/{scheduleId}";
            return DeleteAsync<StatusCodeResult>(url);
        }

        public Task<ScheduleDto> UpdateSchedule(Guid scheduleId, ScheduleDto dto)
        {
            string url = $"api/v1/schedule/{scheduleId}";
            return PutAsync<ScheduleDto, ScheduleDto>(url, dto);
        }

        public Task<DietPropertiesDto> SaveDietProperties(DietPropertiesDto dto)
        {
            string url = $"api/v1/diet/properties";
            return PostAsync<DietPropertiesDto, DietPropertiesDto>(url, dto);
        }

        public Task<DietPropertiesDto> GetDietProperties(string date)
        {
            string url = $"api/v1/diet/properties/{date}";
            return GetAsync<DietPropertiesDto>(url);
        }

        public Task<FoodDto> AddFoodToDiet(AddNewFoodDto dto)
        {
            string url = $"api/v1/diet/food";
            return PostAsync<FoodDto, AddNewFoodDto>(url, dto);
        }

        public Task<List<string>> GetAutocompleteFoodNameList(string foodName)
        {
            string url = $"api/v1/diet/food?foodName={foodName}";
            return GetAsync<List<string>>(url);
        }

        public Task<FoodDto> UpdateFood(Guid id, UpdateFoodDto dto)
        {
            string url = $"api/v1/diet/food/{id}";
            return PutAsync<FoodDto, UpdateFoodDto>(url, dto);
        }

        public Task DeleteFood(Guid id, string date)
        {
            string url = $"api/v1/diet/food/{id}?date={date}";
            return DeleteAsync<StatusCodeResult>(url);
        }

        public Task<DietDto> GetDietByDate(string date)
        {
            string url = $"api/v1/diet/{date}";
            return GetAsync<DietDto>(url);
        }

        public Task<DietSummary> GetDietSummaryByDate(string date)
        {
            string url = $"api/v1/diet/{date}/summary";
            return GetAsync<DietSummary>(url);
        }

        public Task<DashboardSummary> GetDashboardSummaryByDate(string date)
        {
            string url = $"api/v1/summary/{date}";
            return GetAsync<DashboardSummary>(url);
        }

        public Task<IEnumerable<ActivityMonthDto>> GetBurnedCaloriesByMonth(string month, string year)
        {
            string url = $"api/v1/analysis/activity/month/{month}/year/{year}";
            return GetAsync<IEnumerable<ActivityMonthDto>>(url);
        }

        public Task<IEnumerable<ActivityYearDto>> GetBurnedCaloriesByYear(string year)
        {
            string url = $"api/v1/analysis/activity/year/{year}";
            return GetAsync<IEnumerable<ActivityYearDto>>(url);
        }

        public Task<IEnumerable<ActivityMonthDto>> GetBurnedCaloriesByRange(GetGraphCriteria criteria)
        {
            string url = $"api/v1/analysis/activity?startDate={criteria.StartDate}&endDate={criteria.EndDate}";
            return GetAsync<IEnumerable<ActivityMonthDto>>(url);
        }

        public Task<IEnumerable<WeightMonthDto>> GetWeightByRange(GetGraphCriteria criteria)
        {
            string url = $"api/v1/analysis/weight?startDate={criteria.StartDate}&endDate={criteria.EndDate}";
            return GetAsync<IEnumerable<WeightMonthDto>>(url);
        }

        public Task<IEnumerable<WeightMonthDto>> GetWeightByMonth(string month, string year)
        {
            string url = $"api/v1/analysis/weight/month/{month}/year/{year}";
            return GetAsync<IEnumerable<WeightMonthDto>>(url);
        }

        public Task<IEnumerable<DietMonthDto>> GetCaloriesByMonth(string month, string year)
        {
            string url = $"api/v1/analysis/diet/month/{month}/year/{year}";
            return GetAsync<IEnumerable<DietMonthDto>>(url);
        }

        public Task<IEnumerable<DietMonthDto>> GetCaloriesByRange(GetGraphCriteria criteria)
        {
            string url = $"api/v1/analysis/diet?startDate={criteria.StartDate}&endDate={criteria.EndDate}";
            return GetAsync<IEnumerable<DietMonthDto>>(url);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_disposed)
            {
                return;
            }
            if (disposing)
            {
                HttpClient?.Dispose();
                _disposed = true;
            }
        }

    }
}