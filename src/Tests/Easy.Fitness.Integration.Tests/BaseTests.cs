﻿using System;
using System.Threading.Tasks;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Authorization;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Integration.Tests.Configuration;
using Easy.Fitness.Integration.Tests.WebClients;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Integration.Tests
{
    internal class BaseTests
    {
        protected ApiWebClient Client { get; private set; }
        private UserTokenProvider _tokenProvider;
        protected TestsConfiguration Config { get; set; }
        private Mock<IAuthorizationContext> _authorizationContext;
        private Server _server;

        [OneTimeSetUp]
        public virtual async Task OneTimeSetUpAsync()
        {
            _server = new Server();
            await _server.StartAsync();
            Config = ConfigurationHelper.Create<TestsConfiguration>();
            _authorizationContext = new Mock<IAuthorizationContext>();
            _tokenProvider = new UserTokenProvider(Config.Auth);
            _tokenProvider.SetUserCredentials(
                Guid.Parse(Config.Auth.User.Id),
                Config.Auth.User.Email,
                Config.Auth.User.Password);
            Client = new ApiWebClient(_server.Client, _authorizationContext.Object);
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {
            _server?.Dispose();
        }

        protected void GivenAuthorized()
        {
            GivenAuthorized(Config.Auth.User);
        }

        protected void GivenAuthorized(User user)
        {
            _tokenProvider.SetUserCredentials(
                Guid.Parse(user.Id),
                user.Email,
                user.Password);
            string token = _tokenProvider.GetAccessToken();
            _authorizationContext.Setup(x => x.GetAccessTokenAsync()).ReturnsAsync(token);
        }

        protected void GivenUnauthorized()
        {
            _authorizationContext.Setup(x => x.GetAccessTokenAsync()).ReturnsAsync(string.Empty);
        }
    }
}