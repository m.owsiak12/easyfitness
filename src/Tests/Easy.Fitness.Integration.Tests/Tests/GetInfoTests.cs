﻿using System;
using System.Net;
using System.Threading.Tasks;
using Easy.Fitness.Infrastructure.Exceptions;
using Easy.Fitness.Integration.Tests.Models.Dto;
using NUnit.Framework;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class GetInfoTests : BaseTests
    {
        [Test]
        public void Should_return_unauthorized_when_calls_application_version_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetInfo());
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_return_application_version()
        {
            // Arrange
            GivenAuthorized();

            // Act
            ComponentInfo info = await Client.GetInfo();

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotEmpty(info.Version);
            Assert.Greater(Version.Parse(info.Version), Version.Parse("0.0.0.0"));
            CollectionAssert.AreEqual(new string[] { "1" }, info.SupportedApiVersions);
        }

        [Test]
        public async Task Should_return_ok_for_health()
        {
            // Arrange
            GivenAuthorized();

            // Act
            var dto = await Client.GetHealth();

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(dto);
        }

        [Test]
        public async Task Should_return_ok_for_ready()
        {
            // Arrange
            GivenAuthorized();

            // Act
            var dto = await Client.GetReady();

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(dto);
        }
    }
}