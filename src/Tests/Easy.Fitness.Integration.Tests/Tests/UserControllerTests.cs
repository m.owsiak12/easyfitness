﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.User;
using Easy.Fitness.Domain;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Authorization;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Exceptions;
using Easy.Fitness.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class UserControllerTests : BaseTests
    {
        private AppDbContext _context;
        private IUserRepository _userRepository;

        [SetUp]
        public void Setup()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new(optionsBuilder.Options);
            _context.Database.Migrate();
            _userRepository = new UserRepository(_context);
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_user_info_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetUserInfo());
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_return_user_info()
        {
            // Arrange
            GivenAuthorized();

            // Act
            UserInfoDto result = await Client.GetUserInfo();

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Id.ToString(), Is.EqualTo("408655a3-0b0e-4850-a646-149518138fce"));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_update_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.UpdateUser(new UserInfoDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_update_user()
        {
            // Arrange
            GivenAuthorized();
            UserInfoDto dto = new()
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                PhoneNumber = "+48123123123"
            };

            // Act
            UserInfoDto result = await Client.UpdateUser(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.FirstName, Is.EqualTo(dto.FirstName));
            Assert.That(result.LastName, Is.EqualTo(dto.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(dto.PhoneNumber));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_change_password_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.ChangeUserPassword(new ChangePasswordDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        [Ignore("")]
        public async Task Should_change_user_password()
        {
            // Arrange
            GivenAuthorized();
            ChangePasswordDto dto = new()
            {
                CurrentPassword = "NewPassword123",
                NewPassword = "!2Qwerty123"
            };

            // Act
            await Client.ChangeUserPassword(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_user_parameters()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.ChangeUserPassword(new ChangePasswordDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_get_user_parameters()
        {
            // Arrange
            GivenAuthorized();
            UserParametersDto dto = new()
            {
                Height = 180,
                Weight = 80
            };
            await Client.SaveUserParameters(dto);

            // Act
            UserParametersDto result = await Client.GetUserParameters();

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Height, Is.EqualTo(dto.Height));
            Assert.That(result.Weight, Is.EqualTo(dto.Weight));
        }

        [Test]
        public void Should_return_uanuthorized_when_calls_save_user_parameters_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.SaveUserParameters(new UserParametersDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));

        }

        [Test]
        public async Task Should_save_user_parameters()
        {
            // Arrange
            GivenAuthorized();
            UserParametersDto dto = new()
            {
                Weight = 75,
                Height = 180
            };

            // Act
            UserParametersDto result = await Client.SaveUserParameters(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Weight, Is.EqualTo(dto.Weight));
            Assert.That(result.Height, Is.EqualTo(dto.Height));
        }

        [Test]
        public void Should_return_uanuthorized_when_calls_get_personal_info_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetUserPersonalInfo());
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_get_personal_info()
        {
            // Arrange
            GivenAuthorized();
            UserInfoDto dto = new()
            {
                FirstName = "Jan",
                LastName = "Kowalski",
                PhoneNumber = "+48123123123"
            };
            await Client.UpdateUser(dto);

            // Act
            UserAccountDto result = await Client.GetUserPersonalInfo();

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.FirstName, Is.EqualTo(dto.FirstName));
            Assert.That(result.LastName, Is.EqualTo(dto.LastName));
        }

        [Test]
        public async Task Should_authenticate_user()
        {
            // Arrange
            CreateUserDto user = new()
            {
                Email = "abc@abc.pl",
                Password = "!2Qwerty"
            };
            await Client.CreateUser(user);

            // Act
            AccessTokenDto accessToken = await Client.AuthenticateUser(user);
            User createdUser = await _userRepository.GetUserByEmailAsync(user.Email, It.IsAny<CancellationToken>());
            _context.Users.Remove(createdUser);
            _context.SaveChanges();

            // Assert
            Assert.IsNotNull(accessToken);
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task Should_create_user()
        {
            // Arrange
            CreateUserDto user = new()
            {
                Email = "abc@abc.pl",
                Password = "!2Qwerty"
            };

            //Act
            UserDto result = await Client.CreateUser(user);
            User createdUser = await _userRepository.GetUserByEmailAsync(user.Email, It.IsAny<CancellationToken>());

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(createdUser);
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            _context.Users.Remove(createdUser);
            _context.SaveChanges();
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_user_summary_without_authorization()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetUserSummary());
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_return_user_summary()
        {
            // Arrange
            GivenAuthorized();
            ActivityDto activityDto = new()
            {
                Date = "29.04.2024",
                Type = "Gym",
                Name = "gym",
                Calories = 500,
                Duration = "1:00:00"
            };
            ActivityDto activity = await Client.SaveActivity(activityDto);

            // Act
            UserSummary result = await Client.GetUserSummary();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Trainings);
            Assert.AreEqual(activity.Calories, result.Calories);

            await Client.DeleteActivity(activity.Id);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }
    }
}