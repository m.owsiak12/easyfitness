﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos.Diet;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class DietControllerTests : BaseTests
    {
        private AppDbContext _context;

        [SetUp]
        public void Setup()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new(optionsBuilder.Options);
            _context.Database.Migrate();
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Should_return_unauthorized_when_calls_save_diet_properties_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.SaveDietProperties(new DietPropertiesDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_diet_properties_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetDietProperties("2024-04-06"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_add_food_to_diet_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.AddFoodToDiet(new AddNewFoodDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_autocomplete_food_name_list_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetAutocompleteFoodNameList("chic"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_update_food_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.UpdateFood(Guid.NewGuid(), new UpdateFoodDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_delete_food_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.DeleteFood(Guid.NewGuid(), "2024-04-06"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_diet_by_date_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetDietByDate("2024-04-06"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_diet_summary_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetDietSummaryByDate("2024-04-06"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_save_diet_properties()
        {
            // Arrange
            GivenAuthorized();
            DietPropertiesDto dto = new()
            {
                Calories = 3000,
                Fat = 50,
                Carbs = 180,
                Protein = 220,
                Date = "2024-04-07"
            };

            // Act
            DietPropertiesDto result = await Client.SaveDietProperties(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(dto.Date));
            Assert.That(result.Fat, Is.EqualTo(dto.Fat));
            Assert.That(result.Carbs, Is.EqualTo(dto.Carbs));
            Assert.That(result.Protein, Is.EqualTo(dto.Protein));

            DietProperties x = await _context.DietProperties.SingleAsync(x => x.Id == result.Id);
            _context.DietProperties.Remove(x);
            await _context.SaveChangesAsync();
        }

        [Test]
        public async Task Should_get_diet_properties()
        {
            // Arrange
            GivenAuthorized();
            DietPropertiesDto dto = new()
            {
                Calories = 3000,
                Fat = 50,
                Carbs = 180,
                Protein = 220,
                Date = "2024-04-07"
            };
            DietPropertiesDto saved = await Client.SaveDietProperties(dto);

            // Act
            DietPropertiesDto result = await Client.GetDietProperties(saved.Date);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(saved.Date));
            Assert.That(result.Fat, Is.EqualTo(saved.Fat));
            Assert.That(result.Carbs, Is.EqualTo(saved.Carbs));
            Assert.That(result.Protein, Is.EqualTo(saved.Protein));

            DietProperties x = await _context.DietProperties.SingleAsync(x => x.Id == result.Id);
            _context.DietProperties.Remove(x);
            await _context.SaveChangesAsync();
        }

        [Test]
        public async Task Should_add_food_to_diet()
        {
            // Arrange
            GivenAuthorized();
            AddNewFoodDto dto = new()
            {
                Date = "2024-04-07",
                Name = "Chicken",
                Weight = 100,
                Type = "Dinner"
            };

            // Act
            FoodDto result = await Client.AddFoodToDiet(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Name, Is.EqualTo(dto.Name));
            Assert.That(result.Weight, Is.EqualTo(dto.Weight));
            Assert.That(result.Type, Is.EqualTo(dto.Type));

            Food food = await _context.Food.SingleAsync(x => x.Id == result.Id);
            Diet diet = await _context.Diet.Where(x => x.Foods.Contains(food)).SingleAsync();
            _context.Diet.Remove(diet);
            await _context.SaveChangesAsync();
        }

        [Test]
        public async Task Should_get_food_name_list()
        {
            // Arrange
            GivenAuthorized();
            string foodName = "chic";

            // Act
            List<string> result = await Client.GetAutocompleteFoodNameList(foodName);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Contains("chicken"));
        }

        [Test]
        public async Task Should_update_food()
        {
            // Arrange
            GivenAuthorized();
            AddNewFoodDto dto = new()
            {
                Date = "2024-04-07",
                Name = "Chicken",
                Weight = 100,
                Type = "Dinner"
            };
            FoodDto saved = await Client.AddFoodToDiet(dto);
            UpdateFoodDto update = new()
            {
                Date = "2024-04-07",
                Name = "Chicken",
                Type = "Breakfast",
                Weight = 200
            };

            // Act
            FoodDto result = await Client.UpdateFood(saved.Id, update);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Type, Is.EqualTo(update.Type));
            Assert.That(result.Weight, Is.EqualTo(update.Weight));

            Food food = await _context.Food.SingleAsync(x => x.Id == result.Id);
            Diet diet = await _context.Diet.Where(x => x.Foods.Contains(food)).SingleAsync();
            _context.Diet.Remove(diet);
            await _context.SaveChangesAsync();
        }

        [Test]
        public async Task Should_delete_food()
        {
            // Arrange
            GivenAuthorized();
            AddNewFoodDto dto = new()
            {
                Date = "2024-04-07",
                Name = "Chicken",
                Weight = 100,
                Type = "Dinner"
            };
            FoodDto saved = await Client.AddFoodToDiet(dto);

            // Act
            await Client.DeleteFood(saved.Id, dto.Date);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public async Task Should_get_diet_by_date()
        {
            // Arrange
            GivenAuthorized();
            AddNewFoodDto dto = new()
            {
                Date = "2024-04-07",
                Name = "Chicken",
                Weight = 100,
                Type = "Dinner"
            };
            FoodDto saved = await Client.AddFoodToDiet(dto);

            // Act
            DietDto result = await Client.GetDietByDate(dto.Date);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.IsTrue(result.Foods.Count > 0);
            Assert.That(result.Calories, Is.EqualTo(saved.Calories));
            Assert.That(result.Carbs, Is.EqualTo(saved.Carbs));
            Assert.That(result.Protein, Is.EqualTo(saved.Protein));
            Assert.That(result.Fat, Is.EqualTo(saved.Fat));

            Food food = await _context.Food.SingleAsync(x => x.Id == saved.Id);
            Diet diet = await _context.Diet.Where(x => x.Foods.Contains(food)).SingleAsync();
            _context.Diet.Remove(diet);
            await _context.SaveChangesAsync();
        }

        [Test]
        public async Task Should_get_diet_summary()
        {
            // Arrange
            GivenAuthorized();
            AddNewFoodDto dto = new()
            {
                Date = "2024-04-07",
                Name = "Chicken",
                Weight = 100,
                Type = "Dinner"
            };
            FoodDto saved = await Client.AddFoodToDiet(dto);
            DietPropertiesDto dtoDP = new()
            {
                Calories = 3000,
                Fat = 50,
                Carbs = 180,
                Protein = 220,
                Date = "2024-04-07"
            };
            DietPropertiesDto savedDP = await Client.SaveDietProperties(dtoDP);

            // Act
            DietSummary result = await Client.GetDietSummaryByDate(dto.Date);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.CurrentCalories, Is.EqualTo(saved.Calories));
            Assert.That(result.MaxCalories, Is.EqualTo(savedDP.Calories));
            Assert.That(result.CurrentFat, Is.EqualTo(saved.Fat));
            Assert.That(result.MaxFat, Is.EqualTo(savedDP.Fat));
            Assert.That(result.CurrentCarbs, Is.EqualTo(saved.Carbs));
            Assert.That(result.MaxCarbs, Is.EqualTo(savedDP.Carbs));
            Assert.That(result.CurrentProtein, Is.EqualTo(saved.Protein));
            Assert.That(result.MaxProtein, Is.EqualTo(savedDP.Protein));

            Food food = await _context.Food.SingleAsync(x => x.Id == saved.Id);
            Diet diet = await _context.Diet.Where(x => x.Foods.Contains(food)).SingleAsync();
            _context.Diet.Remove(diet);
            await _context.SaveChangesAsync();
        }
    }
}