﻿using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Schedule;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Net;
using System.Threading.Tasks;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class SummaryControllerTests : BaseTests
    {
        private AppDbContext _context;

        [SetUp]
        public void SetUp()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new(optionsBuilder.Options);
            _context.Database.Migrate();
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_summary_without_authorization()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetDashboardSummaryByDate("30.04.2024"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_get_dashboard_summary()
        {
            // Arrange
            GivenAuthorized();
            string date = "30.04.2024";
            ActivityDto activityDto = new()
            {
                Date = "29.04.2024",
                Type = "Gym",
                Name = "gym",
                Calories = 500,
                Duration = "1:00:00"
            };
            ScheduleDto scheduleDto = new()
            {
                Date = "30.04.2024",
                Type = "Swimming",
                Note = "testt"
            };
            ActivityDto activity = await Client.SaveActivity(activityDto);
            ScheduleDto schedule = await Client.SaveSchedule(scheduleDto);

            // Act
            DashboardSummary result = await Client.GetDashboardSummaryByDate(date);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(activity.Type, result.ActivityType);
            Assert.AreEqual(activity.Date, result.ActivityDate);
            Assert.AreEqual(schedule.Type, result.ScheduleType);
            Assert.AreEqual(schedule.Date, result.ScheduleDate);
            Assert.IsNotNull(result.DietSummary);

            await Client.DeleteActivity(activity.Id);
            await Client.DeleteSchedule(schedule.Id);
        }
    }
}
