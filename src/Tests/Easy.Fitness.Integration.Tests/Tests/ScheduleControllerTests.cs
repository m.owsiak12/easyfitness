﻿using System;
using System.Net;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Dtos.Schedule;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class ScheduleControllerTests : BaseTests
    {
        private AppDbContext _context;

        [SetUp]
        public void Setup()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new(optionsBuilder.Options);
            _context.Database.Migrate();
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Should_return_unauthorized_when_calls_save_schedule_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.SaveSchedule(new ScheduleDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_save_schedule()
        {
            // Arrange
            GivenAuthorized();
            ScheduleDto dto = CreateScheduleDto();

            // Act
            ScheduleDto result = await Client.SaveSchedule(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(dto.Date));
            Assert.That(result.Type, Is.EqualTo(dto.Type));
            Assert.That(result.Note, Is.EqualTo(dto.Note));
            await DeleteSchedule(result.Id);
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_schedule_without_login()
        {
            // Arrange
            GivenUnauthorized();
            GetPageCriteria criteria = new()
            {
                Count = 5,
                IsDescending = true,
                Page = 1,
                SortColumn = "date",
                SearchType = "Other",
                SearchDate = "24.03.2024"
            };


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetSchedule(criteria));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_get_page_of_schedule()
        {
            // Arrange
            GivenAuthorized();
            ScheduleDto dto = CreateScheduleDto();
            ScheduleDto savedSchedule = await Client.SaveSchedule(dto);
            GetPageCriteria criteria = new()
            {
                Count = 5,
                IsDescending = true,
                Page = 1,
                SortColumn = "date",
                SearchType = "Other",
                SearchDate = "24.03.2024"
            };

            // Act
            PageDto<ScheduleDto> result = await Client.GetSchedule(criteria);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Items.Count, Is.EqualTo(1));
            Assert.That(result.Items[0].Date, Is.EqualTo(dto.Date));
            Assert.That(result.Items[0].Type, Is.EqualTo(dto.Type));
            Assert.That(result.Items[0].Note, Is.EqualTo(dto.Note));
            await DeleteSchedule(savedSchedule.Id);
        }

        [Test]
        public void Should_return_unauthorized_when_calls_delete_schedule_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.DeleteSchedule(Guid.NewGuid()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_delete_schedule()
        {
            // Arrange
            GivenAuthorized();
            ScheduleDto dto = CreateScheduleDto();
            ScheduleDto result = await Client.SaveSchedule(dto);

            // Act
            await Client.DeleteSchedule(result.Id);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_update_schedule_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.UpdateSchedule(Guid.NewGuid(), new ScheduleDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_update_schedule()
        {
            // Arrange
            GivenAuthorized();
            ScheduleDto dto = CreateScheduleDto();
            ScheduleDto savedSchedule = await Client.SaveSchedule(dto);
            ScheduleDto updateDto = new()
            {
                Date = "25.03.2024",
                Type = "Swimming",
                Note = "testt"
            };

            // Act
            ScheduleDto result = await Client.UpdateSchedule(savedSchedule.Id, updateDto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(savedSchedule.Id));
            Assert.That(result.Date, Is.EqualTo(updateDto.Date));
            Assert.That(result.Type, Is.EqualTo(updateDto.Type));
            Assert.That(result.Note, Is.EqualTo(updateDto.Note));
            await DeleteSchedule(savedSchedule.Id);
        }

        private static ScheduleDto CreateScheduleDto()
        {
            return new ScheduleDto()
            {
                Date = "24.03.2024",
                Type = "Other",
                Note = "test"
            };
        }

        private async Task DeleteSchedule(Guid id)
        {
            await Client.DeleteSchedule(id);
        }
    }
}