﻿using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System.Net;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class AnalysisControllerTests : BaseTests
    {
        private AppDbContext _context;

        [SetUp]
        public void Setup()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new(optionsBuilder.Options);
            _context.Database.Migrate();
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_burned_calories_by_month_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetBurnedCaloriesByMonth("5", "2024"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_burned_calories_by_year_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetBurnedCaloriesByYear("2024"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_burned_calories_by_range_without_login()
        {
            // Arrange
            GivenUnauthorized();
            GetGraphCriteria criteria = new()
            {
                StartDate = "01.05.2024",
                EndDate = "08.05.2024"
            };


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetBurnedCaloriesByRange(criteria));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_weight_by_range_without_login()
        {
            // Arrange
            GivenUnauthorized();
            GetGraphCriteria criteria = new()
            {
                StartDate = "01.05.2024",
                EndDate = "08.05.2024"
            };


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetWeightByRange(criteria));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_weight_by_month_without_login()
        {
            // Arrange
            GivenUnauthorized();


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetWeightByMonth("5", "2024"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_calories_by_month_without_login()
        {
            // Arrange
            GivenUnauthorized();


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetCaloriesByMonth("5", "2024"));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_calories_by_range_without_login()
        {
            // Arrange
            GivenUnauthorized();
            GetGraphCriteria criteria = new()
            {
                StartDate = "01.05.2024",
                EndDate = "08.05.2024"
            };


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetWeightByRange(criteria));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }
    }
}
