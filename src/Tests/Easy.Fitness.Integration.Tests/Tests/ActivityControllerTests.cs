﻿using System;
using System.Net;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Easy.Fitness.Integration.Tests.Tests
{
    internal class ActivityControllerTests : BaseTests
    {
        private AppDbContext _context;

        [SetUp]
        public void Setup()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new(optionsBuilder.Options);
            _context.Database.Migrate();
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test]
        public void Should_return_unauthorized_when_calls_save_activity_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.SaveActivity(new ActivityDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_save_activity()
        {
            // Arrange
            GivenAuthorized();
            ActivityDto dto = CreateActivityDto();

            // Act
            ActivityDto result = await Client.SaveActivity(dto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(dto.Date));
            Assert.That(result.Type, Is.EqualTo(dto.Type));
            Assert.That(result.Name, Is.EqualTo(dto.Name));
            Assert.That(result.Calories, Is.EqualTo(dto.Calories));
            Assert.That(result.Duration, Is.EqualTo(dto.Duration));
            await DeleteActivity(result.Id);
        }

        [Test]
        public void Should_return_unauthorized_when_calls_get_activities_without_login()
        {
            // Arrange
            GivenUnauthorized();
            GetPageCriteria criteria = new()
            {
                Count = 5,
                IsDescending = true,
                Page = 1,
                SortColumn = "date",
                SearchType = "Other",
                SearchDate = "24.03.2024"
            };


            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.GetActivities(criteria));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_get_page_of_activities()
        {
            // Arrange
            GivenAuthorized();
            ActivityDto dto = CreateActivityDto();
            ActivityDto savedActivity = await Client.SaveActivity(dto);
            GetPageCriteria criteria = new()
            {
                Count = 5,
                IsDescending = true,
                Page = 1,
                SortColumn = "date",
                SearchType = "Other",
                SearchDate = "24.03.2024"
            };

            // Act
            PageDto<ActivityDto> result = await Client.GetActivities(criteria);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Items.Count, Is.EqualTo(1));
            Assert.That(result.Items[0].Date, Is.EqualTo(dto.Date));
            Assert.That(result.Items[0].Type, Is.EqualTo(dto.Type));
            Assert.That(result.Items[0].Name, Is.EqualTo(dto.Name));
            Assert.That(result.Items[0].Calories, Is.EqualTo(dto.Calories));
            Assert.That(result.Items[0].Duration, Is.EqualTo(dto.Duration));
            await DeleteActivity(savedActivity.Id);
        }

        [Test]
        public void Should_return_unauthorized_when_calls_delete_activity_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.DeleteActivity(Guid.NewGuid()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_delete_activity()
        {
            // Arrange
            GivenAuthorized();
            ActivityDto dto = CreateActivityDto();
            ActivityDto result = await Client.SaveActivity(dto);

            // Act
            await Client.DeleteActivity(result.Id);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
        }

        [Test]
        public void Should_return_unauthorized_when_calls_update_activity_without_login()
        {
            // Arrange
            GivenUnauthorized();

            // Act & Assert
            Assert.ThrowsAsync<ResponseException>(() => Client.UpdateActivity(Guid.NewGuid(), new ActivityDto()));
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.Unauthorized));
        }

        [Test]
        public async Task Should_update_activity()
        {
            // Arrange
            GivenAuthorized();
            ActivityDto dto = CreateActivityDto();
            ActivityDto savedActivity = await Client.SaveActivity(dto);
            ActivityDto updateDto = new()
            {
                Date = "25.03.2024",
                Type = "Swimming",
                Name = "testt",
                Calories = 700,
                Duration = "2:15:00"
            };

            // Act
            ActivityDto result = await Client.UpdateActivity(savedActivity.Id, updateDto);

            // Assert
            Assert.That(Client.LastStatusCode, Is.EqualTo(HttpStatusCode.OK));
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(savedActivity.Id));
            Assert.That(result.Date, Is.EqualTo(updateDto.Date));
            Assert.That(result.Type, Is.EqualTo(updateDto.Type));
            Assert.That(result.Name, Is.EqualTo(updateDto.Name));
            Assert.That(result.Calories, Is.EqualTo(updateDto.Calories));
            Assert.That(result.Duration, Is.EqualTo(updateDto.Duration));
            await DeleteActivity(savedActivity.Id);
        }

        private static ActivityDto CreateActivityDto()
        {
            return new ActivityDto()
            {
                Date = "24.03.2024",
                Type = "Other",
                Name = "test",
                Calories = 500,
                Duration = "1:00:00"
            };
        }

        private async Task DeleteActivity(Guid id)
        {
            await Client.DeleteActivity(id);
        }
    }
}