﻿using Easy.Fitness.Infrastructure.Configuration;
using Newtonsoft.Json;

namespace Easy.Fitness.Integration.Tests.Configuration
{
    public class Auth : AuthConfiguration
    {
        [JsonRequired]
        public User User { get; set; }
    }
}