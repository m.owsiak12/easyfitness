﻿using Newtonsoft.Json;

namespace Easy.Fitness.Integration.Tests.Configuration
{
    internal class TestsConfiguration
    {
        [JsonRequired]
        public Auth Auth { get; set; }

        [JsonRequired]
        public string ServerAddress { get; set; }
    }
}