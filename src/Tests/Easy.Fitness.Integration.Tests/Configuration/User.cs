﻿using System;
using Newtonsoft.Json;

namespace Easy.Fitness.Integration.Tests.Configuration
{
    public class User
    {
        [JsonRequired]
        public string Id { get; set; }

        [JsonRequired]
        public string Email { get; set; }

        [JsonRequired]
        public string Password { get; set; }
    }
}