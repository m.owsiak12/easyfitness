﻿using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.UserTests
{
    [TestFixture]
    public class UserParametersModelTests
    {
        [Test]
        public void Should_create_user_parameters()
        {
            // Arrange & Act
            UserParameters parameters = new(80, 180);

            // Assert
            Assert.IsNotNull(parameters);
            Assert.That(parameters.Weight, Is.EqualTo(80));
            Assert.That(parameters.Height, Is.EqualTo(180));
        }
    }
}