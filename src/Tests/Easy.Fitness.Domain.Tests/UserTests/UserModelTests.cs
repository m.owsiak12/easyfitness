﻿using System;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.UserTests
{
    [TestFixture]
    public class UserModelTests
    {
        [Test]
        public void Should_create_user_with_email_and_password()
        {
            // Arrange & Act
            User user = new("test@test.pl", "password");

            // Assert
            Assert.IsNotNull(user);
            Assert.That(user.Email, Is.EqualTo("test@test.pl"));
            Assert.That(user.Password, Is.EqualTo("password"));
        }

        [Test]
        public void Should_create_user()
        {
            // Arrange & Act
            User user = new("test@test.pl", "password", "Jan", "Kowalski", "+48123123123", "31.07.2001");

            // Assert
            Assert.IsNotNull(user);
            Assert.That(user.Email, Is.EqualTo("test@test.pl"));
            Assert.That(user.Password, Is.EqualTo("password"));
            Assert.That(user.FirstName, Is.EqualTo("Jan"));
            Assert.That(user.LastName, Is.EqualTo("Kowalski"));
            Assert.That(user.PhoneNumber, Is.EqualTo("+48123123123"));
        }

        [TestCase("", "password")]
        [TestCase("test@test.pl", "")]
        [TestCase(null, null)]
        [TestCase("", null)]
        [TestCase(null, "")]
        public void Should_throw_argument_null_exception_while_creating_user(string email, string password)
        {
            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new User(email, password));
        }
    }
}