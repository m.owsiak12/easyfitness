﻿using System;
using Easy.Fitness.Domain.Models;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.ActivityTests
{
    [TestFixture]
    public class ActivityModelTests
    {
        [Test]
        public void Should_create_activity()
        {
            // Arrange & Act
            Activity activity = new("2024-03-23", "Walking", "Spacer", 430, "2:30:00");

            // Assert
            Assert.IsNotNull(activity);
            Assert.That(activity.Date, Is.EqualTo("2024-03-23"));
            Assert.That(activity.Type, Is.EqualTo("Walking"));
            Assert.That(activity.Name, Is.EqualTo("Spacer"));
            Assert.That(activity.Calories, Is.EqualTo(430));
            Assert.That(activity.Duration, Is.EqualTo("2:30:00"));
        }

        [TestCase(null, "Walking", "Spacer", 430, "2:30:00")]
        [TestCase("2024-03-23", null, "Spacer", 430, "2:30:00")]
        [TestCase("2024-03-23", "Walking", null, 430, "2:30:00")]
        [TestCase("2024-03-23", "Walking", "Spacer", 430, null)]
        [TestCase("", "Walking", "Spacer", 430, "2:30:00")]
        [TestCase("2024-03-23", "", "Spacer", 430, "2:30:00")]
        [TestCase("2024-03-23", "Walking", "", 430, "2:30:00")]
        [TestCase("2024-03-23", "Walking", "Spacer", 430, "")]
        public void Should_throw_argument_null_exception_while_creating_activity(
            string date, string type, string name, double calories, string duration)
        {
            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new Activity(date, type, name, calories, duration));
        }
    }
}