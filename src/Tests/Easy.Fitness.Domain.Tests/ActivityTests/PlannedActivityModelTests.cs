﻿using System;
using Easy.Fitness.Domain.Models;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.ActivityTests
{
    [TestFixture]
    public class PlannedActivityModelTests
    {
        [Test]
        public void Should_create_planned_activity()
        {
            // Arrange & Act
            PlannedActivity plannedActivity = new("23.03.2024", "Football", "piłeczka");

            // Assert
            Assert.IsNotNull(plannedActivity);
            Assert.That(plannedActivity.Date, Is.EqualTo("23.03.2024"));
            Assert.That(plannedActivity.Type, Is.EqualTo("Football"));
            Assert.That(plannedActivity.Note, Is.EqualTo("piłeczka"));
        }

        [TestCase("", "Football", "piłeczka")]
        [TestCase("23.03.2024", "", "piłeczka")]
        [TestCase(null, "Football", "piłeczka")]
        [TestCase("23.03.2024", null, "piłeczka")]
        [TestCase(null, "", "piłeczka")]
        [TestCase(null, null, "")]
        [TestCase("", "", "")]
        public void Should_throw_argument_null_exception_while_creating_planned_activity(
            string date, string type, string note)
        {
            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new PlannedActivity(date, type, note));
        }
    }
}