﻿using System;
using Easy.Fitness.Domain.Models;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.DietTests
{
    [TestFixture]
    public class DietModelTests
    {
        [Test]
        public void Should_create_diet()
        {
            // Arrange & Act
            Diet diet = new("2024-04-06", 3500, 70, 200, 250);

            // Assert
            Assert.IsNotNull(diet);
            Assert.That(diet.Date, Is.EqualTo("2024-04-06"));
            Assert.That(diet.Calories, Is.EqualTo(3500));
            Assert.That(diet.Fat, Is.EqualTo(70));
            Assert.That(diet.Carbs, Is.EqualTo(200));
            Assert.That(diet.Protein, Is.EqualTo(250));
        }

        [TestCase(null, 3500, 70, 200, 250)]
        [TestCase("", 3500, 70, 200, 250)]
        public void Should_throw_argument_null_exception_while_creating_diet(
            string date,
            double calories,
            double fat,
            double carbs,
            double protein)
        {
            // Act && Assert
            Assert.Throws<ArgumentNullException>(() => new Diet(date, calories, fat, carbs, protein));
        }
    }
}