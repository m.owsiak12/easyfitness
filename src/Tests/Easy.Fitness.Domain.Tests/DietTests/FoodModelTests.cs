﻿using System;
using Easy.Fitness.Domain.Models;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.DietTests
{
    [TestFixture]
    public class FoodModelTests
    {
        [Test]
        public void Should_create_food()
        {
            // Arrange & Act
            Food food = new("chicken", 560, 12, 30, 45, 250, "dinner");

            // Assert
            Assert.IsNotNull(food);
            Assert.That(food.Name, Is.EqualTo("chicken"));
            Assert.That(food.Calories, Is.EqualTo(560));
            Assert.That(food.Fat, Is.EqualTo(12));
            Assert.That(food.Carbs, Is.EqualTo(30));
            Assert.That(food.Protein, Is.EqualTo(45));
            Assert.That(food.Weight, Is.EqualTo(250));
            Assert.That(food.Type, Is.EqualTo("dinner"));
        }

        [TestCase(null, 560, 12, 30, 45, 250, "dinner")]
        [TestCase("", 560, 12, 30, 45, 250, "dinner")]
        [TestCase("chicken", 560, 12, 30, 45, 250, null)]
        [TestCase("chicken", 560, 12, 30, 45, 250, "")]
        [TestCase("", 560, 12, 30, 45, 250, "")]
        [TestCase(null, 560, 12, 30, 45, 250, null)]
        public void Should_throw_argument_null_exception_while_creating_food(
            string name,
            double calories,
            double fat,
            double carbs,
            double protein,
            double weight,
            string type)
        {
            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new Food(name, calories, fat, carbs, protein, weight, type));
        }
    }
}