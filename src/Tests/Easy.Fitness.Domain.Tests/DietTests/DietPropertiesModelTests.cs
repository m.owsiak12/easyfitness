﻿using System;
using Easy.Fitness.Domain.Models;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.DietTests
{
    [TestFixture]
    public class DietPropertiesModelTests
    {
        [Test]
        public void Should_create_diet_properties()
        {
            // Arrange & Act
            DietProperties dietProperties = new("2024-04-06", 3500, 70, 200, 250);

            // Assert
            Assert.IsNotNull(dietProperties);
            Assert.That(dietProperties.Date, Is.EqualTo("2024-04-06"));
            Assert.That(dietProperties.Calories, Is.EqualTo(3500));
            Assert.That(dietProperties.Fat, Is.EqualTo(70));
            Assert.That(dietProperties.Carbs, Is.EqualTo(200));
            Assert.That(dietProperties.Protein, Is.EqualTo(250));
        }

        [TestCase(null, 3500, 70, 200, 250)]
        [TestCase("", 3500, 70, 200, 250)]
        public void Should_throw_argument_null_exception_while_creating_diet_properties(
           string date,
           double calories,
           double fat,
           double carbs,
           double protein)
        {
            // Act && Assert
            Assert.Throws<ArgumentNullException>(() => new DietProperties(date, calories, fat, carbs, protein));
        }
    }
}