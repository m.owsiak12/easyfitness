﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Easy.Fitness.Domain.Models;
using NUnit.Framework;

namespace Easy.Fitness.Domain.Tests.DietTests
{
    [TestFixture]
    public class FoodDetailsModelTests
    {
        [Test]
        public void Should_create_food_details()
        {
            // Arrange & Act
            FoodDetails foodDetails = new("chicken", 560, 12, 30, 45, 250);

            // Assert
            Assert.IsNotNull(foodDetails);
            Assert.That(foodDetails.Name, Is.EqualTo("chicken"));
            Assert.That(foodDetails.Calories, Is.EqualTo(560));
            Assert.That(foodDetails.Fat, Is.EqualTo(12));
            Assert.That(foodDetails.Carbs, Is.EqualTo(30));
            Assert.That(foodDetails.Protein, Is.EqualTo(45));
            Assert.That(foodDetails.Weight, Is.EqualTo(250));
        }

        [TestCase(null, 560, 12, 30, 45, 250)]
        [TestCase("", 560, 12, 30, 45, 250)]
        public void Should_throw_argument_null_exception_while_creating_food(
            string name,
            double calories,
            double fat,
            double carbs,
            double protein,
            double weight)
        {
            // Act & Assert
            Assert.Throws<ArgumentNullException>(() => new FoodDetails(name, calories, fat, carbs, protein, weight));
        }
    }
}