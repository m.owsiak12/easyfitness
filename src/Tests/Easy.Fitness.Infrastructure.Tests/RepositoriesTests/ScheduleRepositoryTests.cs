﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Domain.Ids;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Infrastructure.Tests.RepositoriesTests
{
    [TestFixture]
    public class ScheduleRepositoryTests
    {
        private AppDbContext _context;
        private IScheduleRepository _scheduleRepository;
        private Mock<IUserContext> _userContextMock;
        private const string TEST_USER_ID = "408655a3-0b0e-4850-a646-149518138fce";

        [SetUp]
        public void Setup()
        {
            _userContextMock = new Mock<IUserContext>();
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new AppDbContext(optionsBuilder.Options);
            _context.Database.Migrate();
            _scheduleRepository = new ScheduleRepository(_context, _userContextMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test, Isolated]
        public async Task Should_add_schedule()
        {
            // Arrange
            PlannedActivity schedule = CreateSchedule();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));

            // Act
            PlannedActivity result = await _scheduleRepository.SaveNewScheduleAsync(schedule, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(schedule.Date));
            Assert.That(result.Type, Is.EqualTo(schedule.Type));
            Assert.That(result.Note, Is.EqualTo(schedule.Note));
        }

        [Test, Isolated]
        public async Task Should_get_page_of_schedule()
        {
            // Arrange
            PlannedActivity schedule = CreateSchedule();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            await _scheduleRepository.SaveNewScheduleAsync(schedule, CancellationToken.None);
            GetPageCriteria criteria = new()
            {
                Page = 1,
                SortColumn = "date",
                IsDescending = true,
                SearchType = "Other",
                SearchDate = "24.03.2024"
            };

            // Act
            IEnumerable<PlannedActivity> result = await _scheduleRepository.GetSchedulesAsync(criteria.Page, criteria.SortColumn, criteria.IsDescending,
                criteria.SearchType, criteria.SearchDate, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Count(), Is.EqualTo(1));
        }

        [Test, Isolated]
        public async Task Should_get_total_count_of_schedule()
        {
            // Arrange
            PlannedActivity schedule = CreateSchedule();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            await _scheduleRepository.SaveNewScheduleAsync(schedule, CancellationToken.None);

            // Act
            int totalCount = await _scheduleRepository.GetTotalCountAsync(CancellationToken.None);

            // Assert
            Assert.That(totalCount, Is.EqualTo(1));
        }

        [Test, Isolated]
        public async Task Should_update_schedule()
        {
            // Arrange
            PlannedActivity schedule = CreateSchedule();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            PlannedActivity savedSchedule = await _scheduleRepository.SaveNewScheduleAsync(schedule, CancellationToken.None);
            PlannedActivity updateSchedule = new PlannedActivity("25.03.2024", "Running", "Bieganie");

            // Act
            PlannedActivity result = await _scheduleRepository.UpdateScheduleAsync(savedSchedule.Id, updateSchedule, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(savedSchedule.Id));
            Assert.That(result.Date, Is.EqualTo(savedSchedule.Date));
            Assert.That(result.Type, Is.EqualTo(savedSchedule.Type));
            Assert.That(result.Note, Is.EqualTo(savedSchedule.Note));
        }

        [Test, Isolated]
        public async Task Should_delete_schedule()
        {
            // Arrange
            PlannedActivity schedule = CreateSchedule();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            PlannedActivity savedSchedule = await _scheduleRepository.SaveNewScheduleAsync(schedule, CancellationToken.None);
            int totalCountBeforeDeletion = await _scheduleRepository.GetTotalCountAsync(CancellationToken.None);

            // Act
            await _scheduleRepository.DeleteScheduleAsync(savedSchedule.Id, CancellationToken.None);
            int totalCountAfterDeletion = await _scheduleRepository.GetTotalCountAsync(CancellationToken.None);

            // Assert
            Assert.That(totalCountAfterDeletion, Is.EqualTo(totalCountBeforeDeletion - 1));
        }

        private static PlannedActivity CreateSchedule()
        {
            return new PlannedActivity(
                "24.03.2024",
                "Other",
                "test");
        }
    }
}