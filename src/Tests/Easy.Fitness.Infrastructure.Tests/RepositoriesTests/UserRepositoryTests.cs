﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Domain;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace Easy.Fitness.Infrastructure.Tests.RepositoriesTests
{
    [TestFixture]
    public class UserRepositoryTests
    {
        private AppDbContext _context;
        private IUserRepository _userRepository;

        [SetUp]
        public void Setup()
        {
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new AppDbContext(optionsBuilder.Options);
            _context.Database.Migrate();
            _userRepository = new UserRepository(_context);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test, Isolated]
        public async Task Should_add_user()
        {
            // Arrange
            User user = CreateUser();

            // Act
            User result = await _userRepository.AddUserAsync(user, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Email, Is.EqualTo(user.Email));
            Assert.That(result.Password, Is.EqualTo(user.Password));
            Assert.That(result.FirstName, Is.EqualTo(user.FirstName));
            Assert.That(result.LastName, Is.EqualTo(user.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(user.PhoneNumber));
        }

        [Test, Isolated]
        public async Task Should_get_user_by_email()
        {
            // Arrange
            User user = await CreateAndSaveUser();

            // Act
            User result = await _userRepository.GetUserByEmailAsync(user.Email, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Email, Is.EqualTo(user.Email));
            Assert.That(result.Password, Is.EqualTo(user.Password));
            Assert.That(result.FirstName, Is.EqualTo(user.FirstName));
            Assert.That(result.LastName, Is.EqualTo(user.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(user.PhoneNumber));
        }

        [Test, Isolated]
        public async Task Should_update_user()
        {
            // Arrange
            User savedUser = await CreateAndSaveUser();
            User updatedUser = new(savedUser.Id, "Tomasz", "Nowak", "+48123123123", "31.07.2001");

            // Act
            User result = await _userRepository.UpdateUserAsync(updatedUser, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(updatedUser.Id));
            Assert.That(result.FirstName, Is.EqualTo(updatedUser.FirstName));
            Assert.That(result.LastName, Is.EqualTo(updatedUser.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(updatedUser.PhoneNumber));
        }

        [Test, Isolated]
        public async Task Should_get_user_by_id()
        {
            // Arrange
            User savedUser = await CreateAndSaveUser();

            // Act
            User result = await _userRepository.GetUserByIdAsync(savedUser.Id, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(savedUser.Id));
            Assert.That(result.FirstName, Is.EqualTo(savedUser.FirstName));
            Assert.That(result.LastName, Is.EqualTo(savedUser.LastName));
            Assert.That(result.PhoneNumber, Is.EqualTo(savedUser.PhoneNumber));
        }

        [Test, Isolated]
        public async Task Should_update_user_password()
        {
            // Arrange
            User savedUser = await CreateAndSaveUser();
            string newPassword = "NewPassword123";

            // Act
            await _userRepository.UpdateUserPasswordAsync(savedUser.Id, newPassword, CancellationToken.None);

            User result = await _userRepository.GetUserByIdAsync(savedUser.Id, CancellationToken.None);

            // Assert
            Assert.That(result.Password, Is.EqualTo(newPassword));
        }

        [Test, Isolated]
        public async Task Should_save_user_parameters()
        {
            // Arrange
            User user = await CreateAndSaveUser();
            UserParameters parameters = new(80, 180);

            // Act
            UserParameters result = await _userRepository.SaveUserParametersAsync(user.Id, parameters, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Height, Is.EqualTo(parameters.Height));
            Assert.That(result.Weight, Is.EqualTo(parameters.Weight));
        }

        [Test, Isolated]
        public async Task Should_get_latest_user_parameters()
        {
            // Arrange
            User user = CreateUserWithParameters();
            User savedUser = await _userRepository.AddUserAsync(user, CancellationToken.None);

            // Act
            UserParameters result = await _userRepository.GetLatestUserParametersByIdAsync(savedUser.Id, CancellationToken.None);

            UserParameters parameters = savedUser.Parameters.FirstOrDefault();

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Height, Is.EqualTo(parameters.Height));
            Assert.That(result.Weight, Is.EqualTo(parameters.Weight));
        }

        [Test, Isolated]
        public async Task Should_get_user_summary()
        {
            // Arrange
            User user = CreateUser();
            Activity activity = new("01.05.2024", "Gym", "gym", 500, "1:00:00");
            user.Activities = new List<Activity>() { activity };
            User savedUser = await _userRepository.AddUserAsync(user, CancellationToken.None);

            // Act
            UserSummary result = await _userRepository.GetUserSummaryAsync(savedUser.Id, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(activity.Calories, result.Calories);
            Assert.AreEqual(user.Activities.Count, result.Trainings);
        }

        private static User CreateUser()
        {
            return new User(
                "test@test.pl",
                "!2Qwerty",
                "Jan",
                "Kowalski",
                "+48123123123",
                "31.07.2001"
            );
        }

        private static User CreateUserWithParameters()
        {
            User user = CreateUser();
            user.Parameters = new List<UserParameters>()
            {
                { new UserParameters(80, 180)}
            };
            return user;
        }

        private async Task<User> CreateAndSaveUser()
        {
            User user = CreateUser();
            return await _userRepository.AddUserAsync(user, CancellationToken.None);
        }
    }
}