﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Domain.Ids;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Infrastructure.Tests.RepositoriesTests
{
    [TestFixture]
    public class ActivityRepositoryTests
    {
        private AppDbContext _context;
        private IActivityRepository _activityRepository;
        private Mock<IUserContext> _userContextMock;
        private const string TEST_USER_ID = "408655a3-0b0e-4850-a646-149518138fce";

        [SetUp]
        public void Setup()
        {
            _userContextMock = new Mock<IUserContext>();
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new AppDbContext(optionsBuilder.Options);
            _context.Database.Migrate();
            _activityRepository = new ActivityRepository(_context, _userContextMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test, Isolated]
        public async Task Should_add_activity()
        {
            // Arrange
            Activity activity = CreateActivity();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));

            // Act
            Activity result = await _activityRepository.SaveNewActivityAsync(activity, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Date, Is.EqualTo(activity.Date));
            Assert.That(result.Type, Is.EqualTo(activity.Type));
            Assert.That(result.Name, Is.EqualTo(activity.Name));
            Assert.That(result.Calories, Is.EqualTo(activity.Calories));
            Assert.That(result.Duration, Is.EqualTo(activity.Duration));
        }

        [Test, Isolated]
        public async Task Should_get_page_of_activities()
        {
            // Arrange
            Activity activity = CreateActivity();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            await _activityRepository.SaveNewActivityAsync(activity, CancellationToken.None);
            GetPageCriteria criteria = new()
            {
                Page = 1,
                SortColumn = "date",
                IsDescending = true,
                SearchType = "Other",
                SearchDate = "24.03.2024"
            };

            // Act
            IEnumerable<Activity> result = await _activityRepository.GetActivitiesAsync(criteria.Page, criteria.SortColumn, criteria.IsDescending,
                criteria.SearchType, criteria.SearchDate, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Count(), Is.EqualTo(1));
        }

        [Test, Isolated]
        public async Task Should_get_total_count_of_activities()
        {
            // Arrange
            Activity activity = CreateActivity();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            await _activityRepository.SaveNewActivityAsync(activity, CancellationToken.None);

            // Act
            int totalCount = await _activityRepository.GetTotalCountAsync(CancellationToken.None);

            // Assert
            Assert.That(totalCount, Is.EqualTo(1));
        }

        [Test, Isolated]
        public async Task Should_update_activity()
        {
            // Arrange
            Activity activity = CreateActivity();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            Activity savedActivity = await _activityRepository.SaveNewActivityAsync(activity, CancellationToken.None);
            Activity updateActivity = new Activity("25.03.2024", "Running", "Bieganie", 700, "2:00:00");

            // Act
            Activity result = await _activityRepository.UpdateActivityAsync(savedActivity.Id, updateActivity, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(savedActivity.Id));
            Assert.That(result.Date, Is.EqualTo(updateActivity.Date));
            Assert.That(result.Type, Is.EqualTo(updateActivity.Type));
            Assert.That(result.Name, Is.EqualTo(updateActivity.Name));
            Assert.That(result.Calories, Is.EqualTo(updateActivity.Calories));
            Assert.That(result.Duration, Is.EqualTo(updateActivity.Duration));
        }

        [Test, Isolated]
        public async Task Should_delete_activity()
        {
            // Arrange
            Activity activity = CreateActivity();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            Activity savedActivity = await _activityRepository.SaveNewActivityAsync(activity, CancellationToken.None);
            int totalCountBeforeDeletion = await _activityRepository.GetTotalCountAsync(CancellationToken.None);

            // Act
            await _activityRepository.DeleteActivityAsync(savedActivity.Id, CancellationToken.None);
            int totalCountAfterDeletion = await _activityRepository.GetTotalCountAsync(CancellationToken.None);

            // Assert
            Assert.That(totalCountAfterDeletion, Is.EqualTo(totalCountBeforeDeletion - 1));
        }

        private static Activity CreateActivity()
        {
            return new Activity(
                "24.03.2024",
                "Other",
                "Test",
                500,
                "2:15:10");
        }
    }
}