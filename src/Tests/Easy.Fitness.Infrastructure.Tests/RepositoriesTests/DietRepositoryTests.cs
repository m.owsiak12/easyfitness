﻿using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application;
using Easy.Fitness.Domain.Ids;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Repositories;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;

namespace Easy.Fitness.Infrastructure.Tests.RepositoriesTests
{
    [TestFixture]
    public class DietRepositoryTests
    {
        private AppDbContext _context;
        private IDietRepository _dietRepository;
        private Mock<IUserContext> _userContextMock;
        private const string TEST_USER_ID = "408655a3-0b0e-4850-a646-149518138fce";

        [SetUp]
        public void Setup()
        {
            _userContextMock = new Mock<IUserContext>();
            _userContextMock.Setup(x => x.CurrentUserId).Returns(UserId.Parse(TEST_USER_ID));
            AppConfiguration config = ConfigurationHelper.Create<AppConfiguration>();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseNpgsql(config.PostgresDataBase.ConnectionString);
            _context = new AppDbContext(optionsBuilder.Options);
            _context.Database.Migrate();
            _dietRepository = new DietRepository(_context, _userContextMock.Object);
        }

        [TearDown]
        public void TearDown()
        {
            _context.Dispose();
        }

        [Test, Isolated]
        public async Task Should_save_diet_parameters()
        {
            // Arrange
            DietProperties dietProperties = new("2024-04-06", 3000, 50, 180, 220);

            // Act
            DietProperties result = await _dietRepository.SaveDietParametersAsync(dietProperties, CancellationToken.None);
            DietProperties saved = await _dietRepository.GetDietParametersAsync(dietProperties.Date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.IsNotNull(saved);
            Assert.That(result.Date, Is.EqualTo(saved.Date));
            Assert.That(result.Fat, Is.EqualTo(saved.Fat));
            Assert.That(result.Protein, Is.EqualTo(saved.Protein));
            Assert.That(result.Calories, Is.EqualTo(saved.Calories));
            Assert.That(result.Carbs, Is.EqualTo(saved.Carbs));
        }

        [Test, Isolated]
        public async Task Should_get_diet_parameters_by_date()
        {
            // Arrange
            DietProperties dietProperties = new("2024-04-06", 3000, 50, 180, 220);
            DietProperties result = await _dietRepository.SaveDietParametersAsync(dietProperties, CancellationToken.None);

            // Act
            DietProperties saved = await _dietRepository.GetDietParametersAsync(dietProperties.Date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(saved);
            Assert.That(saved.Date, Is.EqualTo(result.Date));
            Assert.That(saved.Fat, Is.EqualTo(result.Fat));
            Assert.That(saved.Protein, Is.EqualTo(result.Protein));
            Assert.That(saved.Calories, Is.EqualTo(result.Calories));
            Assert.That(saved.Carbs, Is.EqualTo(result.Carbs));
        }

        [Test, Isolated]
        public async Task Should_add_new_food_to_diet()
        {
            // Arrange
            string date = "2024-04-06";
            Food food = new("Chicken", 250, 6, 25, 34, 100, "Dinner");

            // Act
            Food result = await _dietRepository.AddNewFoodToDietAsync(food, date, CancellationToken.None);
            int count = await _context.Food.CountAsync();

            // Assert
            Assert.IsNotNull(result);
            Assert.That(count, Is.EqualTo(1));
            Assert.That(result.Name, Is.EqualTo(food.Name));
            Assert.That(result.Calories, Is.EqualTo(food.Calories));
            Assert.That(result.Carbs, Is.EqualTo(food.Carbs));
            Assert.That(result.Fat, Is.EqualTo(food.Fat));
            Assert.That(result.Protein, Is.EqualTo(food.Protein));
            Assert.That(result.Type, Is.EqualTo(food.Type));
            Assert.That(result.Weight, Is.EqualTo(food.Weight));
        }

        [Test, Isolated]
        public async Task Should_delete_food()
        {
            // Arrange
            string date = "2024-04-06";
            Food food = new("Chicken", 250, 6, 25, 34, 100, "Dinner");
            Food saved = await _dietRepository.AddNewFoodToDietAsync(food, date, CancellationToken.None);
            int totalCountBeforeDeletion = await _context.Food.CountAsync();

            // Act
            await _dietRepository.DeleteFoodAsync(saved.Id, date, CancellationToken.None);
            int totalCountAfterDeletion = await _context.Food.CountAsync();

            // Assert
            Assert.That(totalCountAfterDeletion, Is.EqualTo(totalCountBeforeDeletion - 1));
        }

        [Test, Isolated]
        public async Task Should_update_food()
        {
            // Arrange 
            string date = "2024-04-06";
            Food food = new("Chicken", 250, 6, 25, 34, 100, "Dinner");
            Food saved = await _dietRepository.AddNewFoodToDietAsync(food, date, CancellationToken.None);
            Food updateFood = new Food("Chicken", 500, 12, 50, 68, 200, "Dinner");

            // Act
            Food result = await _dietRepository.UpdateFoodAsync(saved.Id, date, updateFood, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.Id, Is.EqualTo(saved.Id));
            Assert.That(result.Name, Is.EqualTo(updateFood.Name));
            Assert.That(result.Calories, Is.EqualTo(updateFood.Calories));
            Assert.That(result.Fat, Is.EqualTo(updateFood.Fat));
            Assert.That(result.Carbs, Is.EqualTo(updateFood.Carbs));
            Assert.That(result.Weight, Is.EqualTo(updateFood.Weight));
            Assert.That(result.Type, Is.EqualTo(updateFood.Type));
        }

        [Test, Isolated]
        public async Task Should_get_diet_by_date()
        {
            // Arrange
            DietProperties dietProperties = new("2024-04-06", 3000, 50, 180, 220);
            await _dietRepository.SaveDietParametersAsync(dietProperties, CancellationToken.None);
            string date = "2024-04-06";
            Food food = new("Chicken", 250, 6, 25, 34, 100, "Dinner");
            await _dietRepository.AddNewFoodToDietAsync(food, date, CancellationToken.None);

            // Act
            Diet diet = await _dietRepository.GetDietByDateAsync(date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(diet);
            Assert.That(diet.Date, Is.EqualTo(date));
            Assert.That(diet.Calories, Is.EqualTo(food.Calories));
            Assert.That(diet.Fat, Is.EqualTo(food.Fat));
            Assert.That(diet.Carbs, Is.EqualTo(food.Carbs));
            Assert.That(diet.Protein, Is.EqualTo(food.Protein));
            Assert.That(diet.Foods.Count, Is.EqualTo(1));
            Assert.IsNotNull(diet.Properties);
        }

        [Test, Isolated]
        public async Task Should_get_diet_summary_by_date()
        {
            // Arrange
            DietProperties dietProperties = new("2024-04-06", 3000, 50, 180, 220);
            await _dietRepository.SaveDietParametersAsync(dietProperties, CancellationToken.None);
            string date = "2024-04-06";
            Food food = new("Chicken", 250, 6, 25, 34, 100, "Dinner");
            await _dietRepository.AddNewFoodToDietAsync(food, date, CancellationToken.None);

            // Act
            DietSummary result = await _dietRepository.GetDietSummaryByDateAsync(date, CancellationToken.None);

            // Assert
            Assert.IsNotNull(result);
            Assert.That(result.MaxCalories, Is.EqualTo(dietProperties.Calories));
            Assert.That(result.MaxFat, Is.EqualTo(dietProperties.Fat));
            Assert.That(result.MaxCarbs, Is.EqualTo(dietProperties.Carbs));
            Assert.That(result.MaxProtein, Is.EqualTo(dietProperties.Protein));
            Assert.That(result.CurrentCalories, Is.EqualTo(food.Calories));
            Assert.That(result.CurrentCarbs, Is.EqualTo(food.Carbs));
            Assert.That(result.CurrentFat, Is.EqualTo(food.Fat));
            Assert.That(result.CurrentProtein, Is.EqualTo(food.Protein));
        }
    }
}