﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Easy.Fitness.Infrastructure.Migrations
{
    public partial class AddBirthDateColumnInUserTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "BirthDate",
                table: "User",
                type: "character varying(100)",
                maxLength: 100,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BirthDate",
                table: "User");
        }
    }
}
