﻿using System.Threading.Tasks;

namespace Easy.Fitness.Infrastructure
{
    public interface IAuthorizationContext
    {
        Task<string> GetAccessTokenAsync();
    }
}