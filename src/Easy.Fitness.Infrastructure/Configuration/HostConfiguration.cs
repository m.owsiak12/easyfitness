﻿using Newtonsoft.Json;

namespace Easy.Fitness.Infrastructure.Configuration
{
    public class HostConfiguration
    {
        [JsonRequired]
        public string Host { get; set; }
    }
}