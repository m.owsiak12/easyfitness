﻿using Newtonsoft.Json;

namespace Easy.Fitness.Infrastructure.Configuration
{
    public class StorageConfig
    {
        [JsonRequired]
        public string Root { get; set; }

        public MinioConfiguration Minio { get; set; }
    }
}
