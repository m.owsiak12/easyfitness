﻿using Newtonsoft.Json;

namespace Easy.Fitness.Infrastructure.Configuration
{
    public class DataBaseConfig
    {
        [JsonRequired]
        public string ConnectionString { get; set; }
    }
}