describe('Register View Tests', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000/register');
  });

  it('Should allow user to enter credentials and register', () => {
    cy.intercept('POST', '/api/v1/register', {
      statusCode: 200,
      body: {
        id: 'id',
        email: 'test@example.com',
        password: 'password123',
        firstName: null,
        lastName: null,
        phoneNumber: null
      }
    }).as('registerRequest');

    cy.get('input[placeholder="email"]').type('test@example.com');
    cy.get('input[placeholder="password"]').type('password123');
    cy.get('input[placeholder="repeat password"]').type('password123');
    cy.get('button').contains('Sign up').click();
    
    cy.get('div').should('contain', 'Your account has been successfully created');
    cy.url().should('include', "/");
  });

  it('Should redirect to login page after clicking on redirect button', () => {
    cy.get('b').contains('Sign in now').click();
    cy.url().should('include', '');
  });

  it('Should toggle password visibility', () => {
    cy.get('input[placeholder="password"]').type('password123');
    cy.get('input[placeholder="repeat password"]').type('password123');
    cy.get('button.MuiIconButton-root .MuiSvgIcon-root').click({ multiple: true });

    cy.get('input[placeholder="password"]').should('have.attr', 'type', 'text');
    cy.get('input[placeholder="repeat password"]').should('have.attr', 'type', 'text');
  })
});