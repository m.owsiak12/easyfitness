describe('Dashboard View Tests', () => {
  beforeEach(() => {
    const today = new Date();
    const formatDate = (value) => {
      return value < 10 ? `0${value}` : `${value}`;
    };
    const formattedDate = today.getFullYear() + '-' + formatDate(today.getMonth() + 1) + '-' + formatDate(today.getDate());
    localStorage.setItem('token', 'example_token');
    cy.visit('http://localhost:3000/dashboard');
    cy.viewport(1440, 1024);

    cy.intercept('GET', '/api/v1/user/image', {
      statusCode: 200,
      body: {
        fileBytes: null
      }
    }).as('imageRequest');

    cy.intercept('GET', '/api/v1/user', {
      statusCode: 200,
      body: {
        firstName: 'John',
        lastName: 'Doe',
        phoneNumber: '123123123',
        birthDate: '2001-07-31'
      }
    }).as('infoRequest');

    cy.intercept('GET', `/api/v1/summary/${formattedDate}`, {
      statusCode: 200,
      body: {
        dietSummary: {
          currentCalories: 2000,
          maxCalories: 3000,
          currentFat: 50,
          maxFat: 70,
          currentCarbs: 100,
          maxCarbs: 150,
          currentProtein: 120,
          maxProtein: 180
        },
        scheduleType: 'Gym',
        scheduleDate: '2024-05-11',
        activityType: 'Swimming',
        activityDate: '2024-05-08' 
      }
    }).as('summaryRequest');
  });

  it('Should redirect to activity when click on activity navbar', () => {
    cy.contains('Activity').click();
    cy.url().should('include', '/activity');
  });

  it('Should redirect to schedule when click on schedule navbar', () => {
    cy.contains('Schedule').click();
    cy.url().should('include', '/schedule');
  });

  it('Should redirect to diet when click on diet navbar', () => {
    cy.contains('Diet').click();
    cy.url().should('include', '/diet');
  });

  it('Should redirect to progress when click on progress navbar', () => {
    cy.contains('Progress').click();
    cy.url().should('include', '/analysis');
  });

  it('Should logout when click on logout', () => {
    cy.contains('Logout').click();
    cy.url().should('include', '/');
  });

  it('Should redirect to settings when click on settings', () => {
    cy.contains('Settings').click();
    cy.url().should('include', '/settings');
  });

  it('Should redirect to account when click on account', () => {
    cy.contains('Account').click();
    cy.url().should('include', '/account');
  });

  it('Should redirect to activit when click on activity tile', () => {
    cy.contains('Last activity').click();
    cy.url().should('include', '/activity');
  });

  it('Should redirect to schedule when click on schedule tile', () => {
    cy.contains('Next planned activity').click();
    cy.url().should('include', '/schedule');
  });

  it('Should redirect to diet when click on diet tile', () => {
    cy.contains('Carbs').click();
    cy.url().should('include', '/diet');
  });
  
});