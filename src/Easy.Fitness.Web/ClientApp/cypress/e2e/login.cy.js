describe('Login View Tests', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('Should allow user to enter credentials and login', () => {
    cy.intercept('POST', '/api/v1/login', {
      statusCode: 200,
      body: {
        accessToken: 'accessToken'
      }
    }).as('loginRequest');

    cy.get('input[placeholder="email"]').type('test@example.com');
    cy.get('input[placeholder="password"]').type('password123');
    cy.get('button').contains('Sign in').click();

    cy.url().should('include', '/dashboard');
    cy.window().its('localStorage.token').should('be.a', 'string');
  });

  it('Should display an error message for invalid login', () => {
    cy.intercept('POST', '/api/v1/login', {
      statusCode: 400,
      body: 'Invalid credentials'
    }).as('loginRequest');

    cy.get('input[placeholder="email"]').type('test@example.com');
    cy.get('input[placeholder="password"]').type('password123');
    cy.get('button').contains('Sign in').click();

    cy.get('div').should('contain', 'Invalid credentials');
  });

  it('Should toggle password visibility', () => {
    cy.get('input[placeholder="password"]').type('password123');
    cy.get('button.MuiIconButton-root .MuiSvgIcon-root').click();
    cy.get('input[placeholder="password"]').should('have.attr', 'type', 'text');
  });

  it('Should redirect to register page after clicking on redirect button', () => {
    cy.get('b').contains('Sign up now').click();
    cy.url().should('include', '/register');
  });

});