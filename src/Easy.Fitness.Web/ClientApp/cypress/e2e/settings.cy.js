describe('Settings View Tests', () => {
  beforeEach(() => {
    localStorage.setItem('token', 'example_token');
    cy.visit('http://localhost:3000/settings');
    cy.viewport(1440, 1024);

    cy.intercept('GET', '/api/v1/user/image', {
      statusCode: 200,
      body: {
        fileBytes: null
      }
    }).as('imageRequest');

    cy.intercept('GET', '/api/v1/user', {
      statusCode: 200,
      body: {
        firstName: 'John',
        lastName: 'Doe',
        phoneNumber: '123123123',
        birthDate: '31.07.2001'
      }
    }).as('infoRequest');
  });

  it('Should allow user to edit profile', () => {
    cy.intercept('PUT', '/api/v1/user', {
      statusCode: 200,
      body: {
        firstName: 'Jan',
        lastName: 'Kowalski',
        phoneNumber: '123123321',
        birthDate: '24.05.1999'
      }
    }).as('updateUserRequest');

    cy.get('input[placeholder="First name"]').type('Jan');
    cy.get('input[placeholder="Last name"]').type('Kowalski');
    cy.get('input[type="tel"]').type('123123321');
    cy.get('input[type="date"]').type('1999-05-24');
    cy.get('button').contains('Apply changes').click();

    cy.get('div').should('contain', 'Your data has been saved successfully');
  });

  it('Should allow user to change password', () => {
    cy.intercept('PUT', '/api/v1/user/password', {
      statusCode: 200,
      body: {

      }
    }).as('updateUserRequest');

    cy.get('input[placeholder="******"]').each(($el) => {
      if ($el.attr('type') === 'password') {
        cy.wrap($el).type('NewPassword123');
      }
    });
    cy.get('button').last().click();
  }); 
});