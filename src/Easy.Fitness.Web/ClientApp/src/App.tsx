import { Suspense } from 'react';
import './App.css';
import Loading from './components/Loading';
import { Route, Routes } from 'react-router';
import Login from './pages/Login/Login';
import SignUp from './pages/SignUp/SignUp';
import ProtectedRoute from './security/ProtectedRoute';
import Settings from './pages/Settings/Settings';
import Account from './pages/Account/Account';
import Dashboard from './pages/Dashboard/Dashboard';
import Activity from './pages/Activity/Activity';
import Schedule from './pages/Schedule/Schedule';
import Diet from './pages/Diet/Diet';
import Analysis from './pages/Analysis/Analysis';

function App() {
  return (
    <Suspense fallback={<Loading />}>
      <Routes>
        <Route index path="/" element={<Login />} />
        <Route path="/register" element={<SignUp />} />
        <Route path="/dashboard" element={<ProtectedRoute><Dashboard/></ProtectedRoute>} />
        <Route path="/settings" element={<ProtectedRoute><Settings /></ProtectedRoute>} />
        <Route path="/account" element={<ProtectedRoute><Account /></ProtectedRoute>} />
        <Route path="/activity" element={<ProtectedRoute><Activity /></ProtectedRoute>} />
        <Route path="/schedule" element={<ProtectedRoute><Schedule /></ProtectedRoute>} />
        <Route path="/diet" element={<ProtectedRoute><Diet /></ProtectedRoute>} />
        <Route path="/analysis" element={<ProtectedRoute><Analysis /></ProtectedRoute>} />
      </Routes>
    </Suspense>
  )
}

export default App;
