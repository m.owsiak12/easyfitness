import { DietSummaryDto } from './diet';

export interface DashboardSummaryDto {
  dietSummary: DietSummaryDto;
  scheduleType: string | null;
  scheduleDate: string | null;
  activityType: string | null;
  activityDate: string | null;
}