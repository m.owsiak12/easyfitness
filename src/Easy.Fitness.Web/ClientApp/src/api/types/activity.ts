export interface ActivityDto {
  id?: string;
  date: string;
  type: string;
  name: string;
  calories: number;
  duration: string;
}

export interface DurationInterface {
  hours: number;
  minutes: number;
  seconds: number;
}

export interface PageDto<T> {
  items: T[];
  page: number;
  pageSize: number;
  totalCount: number;
  hasNextPage: boolean;
  hasPreviousPage: boolean;
}