export interface ScheduleDto {
  id?: string;
  date: string;
  type: string;
  note: string;
}