export interface LoginDto {
  email: string;
  password: string;
}

export interface RegisterDto {
  email: string;
  password: string;
  repeatPassword: string;
}

export interface UserDto {
  id: string;
  email: string;
  password: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
}

export interface TokenDto {
  accessToken: string;
}

export interface UserInfoDto {
  id?: string;
  firstName?: string;
  lastName?: string;
  phoneNumber?: string;
  birthDate?: string;
}

export interface ChangePasswordDto {
  currentPassword: string;
  newPassword: string;
}

export interface UserParametersDto {
  weight?: number;
  height?: number;
}

export interface UserActivitySummaryDto{
  trainings: number;
  calories: number;
}

export interface UserImageDto {
  fileBytes: string;
}