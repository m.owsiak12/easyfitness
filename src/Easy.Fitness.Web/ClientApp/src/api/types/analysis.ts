export interface BurnedCaloriesMonthDto {
  day: string;
  calories: number;
}

export interface BurnedCaloriesYearDto {
  month: string;
  calories: number;
}

export interface WeightMonthDto {
  day: string;
  weight: number;
}

export interface CaloriesMonthDto {
  day: string;
  calories: number;
}