export interface FoodDto {
  id: string;
  name: string;
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
  weight: number;
  type: string;
}

export interface DayDietDto {
  id?: string;
  date: string;
  calories?: number;
  fat?: number;
  carbs?: number;
  protein?: number;
}

export interface AddFoodDto {
  date: string;
  name: string;
  weight: number;
  type: string;
}

export interface DietDto {
  calories: number;
  fat: number;
  carbs: number;
  protein: number;
  foods: FoodDto[];
}

export interface UpdateFoodDto {
  name: string;
  weight: number;
  type: string;
  date: string;
}

export interface DietSummaryDto {
  currentCalories: number;
  maxCalories: number;
  currentFat: number;
  maxFat: number;
  currentCarbs: number;
  maxCarbs: number;
  currentProtein: number;
  maxProtein: number;
}