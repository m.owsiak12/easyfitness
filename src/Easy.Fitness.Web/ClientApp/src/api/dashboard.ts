import { get } from './axiosSource';
import { CancellationSource } from './models/CancelationSource';
import { DashboardSummaryDto } from './types/dashboard';

export const getDashboardSummary = async (
  date: string,
  cancellationSource?: CancellationSource
): Promise<DashboardSummaryDto> => {
  return get<DashboardSummaryDto>(`api/v1/summary/${date}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};