import { deletion, get, post, put } from './axiosSource';
import { CancellationSource } from './models/CancelationSource';
import { AddFoodDto, DayDietDto, DietDto, DietSummaryDto, FoodDto, UpdateFoodDto } from './types/diet';

export const setDietProperties = async (
  dietConfiguration: DayDietDto,
  cancellationSource?: CancellationSource
): Promise<DayDietDto> => {
  return post<DayDietDto>('api/v1/diet/properties', dietConfiguration, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getDietProperties = async (
  date: string,
  cancellationSource?: CancellationSource
): Promise<DayDietDto> => {
  return get<DayDietDto>(`api/v1/diet/properties/${date}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const addNewFood = async (
  newFood: AddFoodDto,
  cancellationSource?: CancellationSource
): Promise<FoodDto> => {
  return post<FoodDto>(`api/v1/diet/food`, newFood, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getAutocompleteFoodNames = async (
  foodName: string
): Promise<string[]> => {
  return get<string[]>(`api/v1/diet/food`, {
    params: { foodName }
  });
};

export const getDietByDate = async (
  date: string,
  cancellationSource?: CancellationSource
): Promise<DietDto> => {
  return get<DietDto>(`api/v1/diet/${date}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const deleteFood = async (
  id: string,
  date: string,
  cancellationSource?: CancellationSource
): Promise<void> => {
  return deletion<void>(`api/v1/diet/food/${id}`, {
    params: { date },
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const updateFood = async (
  id: string,
  food: UpdateFoodDto,
  cancellationSource?: CancellationSource
): Promise<FoodDto> => {
  return put<FoodDto>(`api/v1/diet/food/${id}`, food, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getDietSummary = async (
  date: string,
  cancellationSource?: CancellationSource
): Promise<DietSummaryDto> => {
  return get<DietSummaryDto>(`api/v1/diet/${date}/summary`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};