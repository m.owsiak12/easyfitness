import { deletion, get, post, put } from './axiosSource';
import { CancellationSource } from './models/CancelationSource';
import { PageDto } from './types/activity';
import { ScheduleDto } from './types/schedule';

export const addNewSchedule = async (
  newSchedule: ScheduleDto,
  cancellationSource?: CancellationSource
): Promise<ScheduleDto> => {
  return post<ScheduleDto>('api/v1/schedule', newSchedule, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getSchedulePage = async (
  count: number,
  isDescending: boolean,
  page: number,
  sortColumn: string,
  searchType?: string,
  searchDate?: string,
  cancellationSource?: CancellationSource
): Promise<PageDto<ScheduleDto>> => {
  return get<PageDto<ScheduleDto>>('api/v1/schedule', {
    params: { count, isDescending, page, sortColumn, searchType, searchDate },
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const deleteSchedule = async (
  id: string,
  cancellationSource?: CancellationSource
): Promise<void> => {
  return deletion<void>(`api/v1/schedule/${id}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const updateSchedule = async (
  id: string,
  schedule: ScheduleDto,
  cancellationSource?: CancellationSource
): Promise<ScheduleDto> => {
  return put<ScheduleDto>(`api/v1/schedule/${id}`, schedule, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};