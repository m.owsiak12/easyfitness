import { get } from './axiosSource';
import { CancellationSource } from './models/CancelationSource';
import { BurnedCaloriesMonthDto, BurnedCaloriesYearDto, CaloriesMonthDto, WeightMonthDto } from './types/analysis';

export const getBurnedCaloriesByMonth = async (
  month: string,
  year: string,
  cancellationSource?: CancellationSource
): Promise<BurnedCaloriesMonthDto[]> => {
  return get<BurnedCaloriesMonthDto[]>(`api/v1/analysis/activity/month/${month}/year/${year}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getBurnedCaloriesByYear = async (
  year: string,
  cancellationSource?: CancellationSource
): Promise<BurnedCaloriesYearDto[]> => {
  return get<BurnedCaloriesYearDto[]>(`api/v1/analysis/activity/year/${year}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getBurnedCaloriesByDateRange = async (
  startDate: string,
  endDate: string,
  cancellationSource?: CancellationSource
): Promise<BurnedCaloriesMonthDto[]> => {
  return get<BurnedCaloriesMonthDto[]>(`api/v1/analysis/activity`, {
    params: { startDate, endDate },
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getWeightByMonth = async (
  month: string,
  year: string,
  cancellationSource?: CancellationSource
): Promise<WeightMonthDto[]> => {
  return get<WeightMonthDto[]>(`api/v1/analysis/weight/month/${month}/year/${year}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getWeightByRange = async (
  startDate: string,
  endDate: string,
  cancellationSource?: CancellationSource
): Promise<WeightMonthDto[]> => {
  return get<WeightMonthDto[]>(`api/v1/analysis/weight`, {
    params: { startDate, endDate },
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getCaloriesByMonth = async (
  month: string,
  year: string,
  cancellationSource?: CancellationSource
): Promise<CaloriesMonthDto[]> => {
  return get<CaloriesMonthDto[]>(`api/v1/analysis/diet/month/${month}/year/${year}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getCaloriesByRange = async (
  startDate: string,
  endDate: string,
  cancellationSource?: CancellationSource
): Promise<CaloriesMonthDto[]> => {
  return get<CaloriesMonthDto[]>(`api/v1/analysis/diet`, {
    params: { startDate, endDate },
    cancelToken: cancellationSource?.tokenSource.token
  });
};