import { deletion, get, post, put } from './axiosSource';
import { CancellationSource } from './models/CancelationSource';
import { ChangePasswordDto, LoginDto, TokenDto, UserActivitySummaryDto, UserDto, UserImageDto, UserInfoDto, UserParametersDto } from './types/user'

export const registerUser = async (
  newUser: LoginDto,
  cancellationSource?: CancellationSource
): Promise<UserDto> => {
  return post<UserDto>('api/v1/register', newUser, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const loginUser = async (
  userCredentials: LoginDto,
  cancellationSource?: CancellationSource
): Promise<TokenDto> => {
  return post<TokenDto>('api/v1/login', userCredentials, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const logoutUser = () => {
  localStorage.removeItem('token');
};

export const getUserInfo = async (
  cancellationSource?: CancellationSource
): Promise<UserInfoDto> => {
  return get<UserInfoDto>('api/v1/user', {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const changePassword = async (
  passwordDto: ChangePasswordDto,
  cancellationSource?: CancellationSource
): Promise<void> => {
  return put<void>('api/v1/user/password', passwordDto, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const updateUser = async (
  userData: UserInfoDto,
  cancellationSource?: CancellationSource
): Promise<UserInfoDto> => {
  return put<UserInfoDto>('api/v1/user', userData, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const updateUserParameters = async (
  userParameters: UserParametersDto,
  cancellationSource?: CancellationSource
): Promise<UserParametersDto> => {
  return post<UserParametersDto>('api/v1/user/parameters', userParameters, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getUserParameters = async (
  cancellationSource?: CancellationSource
): Promise<UserParametersDto> => {
  return get<UserParametersDto>('api/v1/user/parameters', {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getUserAccountInfo = async (
  cancellationSource?: CancellationSource
): Promise<UserInfoDto> => {
  return get<UserInfoDto>('api/v1/user/account', {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getUserActivitySummary = async (
  cancellationSource?: CancellationSource
): Promise<UserActivitySummaryDto> => {
  return get<UserActivitySummaryDto>(`api/v1/user/summary`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const changeUserPicture = async (
  formData: FormData,
  cancellationSource?: CancellationSource
): Promise<void> => {
  const config = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    cancelToken: cancellationSource?.tokenSource.token
  };
  return put<void>('api/v1/user/image', formData, config);
};

export const getUserPicture = async (
  cancellationSource?: CancellationSource
): Promise<UserImageDto> => {
  return get<UserImageDto>('api/v1/user/image', {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const deleteUserPicture = async (
  cancellationSource?: CancellationSource
): Promise<void> => {
  return deletion<void>('api/v1/user/image', {
    cancelToken: cancellationSource?.tokenSource.token
  });
};