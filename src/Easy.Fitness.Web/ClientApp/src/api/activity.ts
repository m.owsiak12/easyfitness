import { deletion, get, post, put } from './axiosSource';
import { CancellationSource } from './models/CancelationSource';
import { ActivityDto, PageDto } from './types/activity';

export const addNewActivity = async (
  newActivity: ActivityDto,
  cancellationSource?: CancellationSource
): Promise<ActivityDto> => {
  return post<ActivityDto>('api/v1/activity', newActivity, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const getActivityPage = async (
  count: number,
  isDescending: boolean,
  page: number,
  sortColumn: string,
  searchType?: string,
  searchDate?: string,
  cancellationSource?: CancellationSource
): Promise<PageDto<ActivityDto>> => {
  return get<PageDto<ActivityDto>>('api/v1/activity', {
    params: { count, isDescending, page, sortColumn, searchType, searchDate },
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const deleteActivity = async (
  id: string,
  cancellationSource?: CancellationSource
): Promise<void> => {
  return deletion<void>(`api/v1/activity/${id}`, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};

export const updateActivity = async (
  id: string,
  activity: ActivityDto,
  cancellationSource?: CancellationSource
): Promise<ActivityDto> => {
  return put<ActivityDto>(`api/v1/activity/${id}`, activity, {
    cancelToken: cancellationSource?.tokenSource.token
  });
};