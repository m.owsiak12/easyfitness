import { DateRangeInterface } from '../AnalysisGraphWorkspace';
import { Dayjs } from 'dayjs';
import { Line } from 'react-chartjs-2';
import { useState, useEffect } from 'react';
import CustomizedSnackbar, { SnackbarInterface } from '../../../../components/CustomizedSnackbar';
import { useCancellationToken } from '../../../../hooks/useCancellationToken';
import { CaloriesMonthDto } from '../../../../api/types/analysis';
import { Error, isCancel } from '../../../../api/axiosSource';
import { getCaloriesByMonth, getCaloriesByRange } from '../../../../api/analysis';
import { Box } from '@mui/material';
import { Chart, CategoryScale, LinearScale, BarElement, PointElement, LineElement, Title, Tooltip, Legend } from 'chart.js';
import CustomizedProgress from '../../../../components/CustomizedProgress';
import { GraphDataInterface } from '../GraphComponent';
import { formatMonth } from '../../../../helpers/date';

Chart.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);

interface DietGraphProps {
  option: string;
  dateRange?: DateRangeInterface;
  month: Dayjs | null;
}

const options = {
  responsive: true,
  plugins: {
    legend: {
      position: 'top' as const,
    },
    title: {
      display: true,
      text: 'Calories'
    },
  },
};

export var data: GraphDataInterface = { labels: [], datasets: [] };

export default function DietGraph({ option, dateRange, month }: DietGraphProps) {
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [snackbar, setSnackbar] = useState<SnackbarInterface>({ open: false, type: undefined, message: '' });
  const [dietMonth, setDietMonth] = useState<CaloriesMonthDto[]>([]);
  const [dietRange, setDietRange] = useState<CaloriesMonthDto[]>([]);

  const cancellation = useCancellationToken();

  const getCaloriesByMonthAction = async (cancelToken: any) => {
    setIsLoading(true);
    return getCaloriesByMonth(
      formatMonth(month),
      month!.year().toString(),
      cancelToken
    )
      .then((items) => {
        setDietMonth(items);
        setIsLoading(false);
      })
      .catch((e: Error) => {
        if (!isCancel(e)) {
          setSnackbar({
            open: true,
            type: "error",
            message: e.response.data
          });
        }
        setIsLoading(false);
      });
  };

  const getCaloriesByRangeAction = async (cancelToken: any) => {
    setIsLoading(true);
    return getCaloriesByRange(
      dateRange?.startDate!,
      dateRange?.endDate!,
      cancelToken
    )
      .then((items) => {
        setDietRange(items);
        setIsLoading(false);
      })
      .catch((e: Error) => {
        if (!isCancel(e)) {
          setSnackbar({
            open: true,
            type: "error",
            message: e.response.data
          });
        }
        setIsLoading(false);
      });
  };

  const handleCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      open: false
    }));
  };

  const getGraphData = async () => {
    cancellation(async (cancelToken) => {
      if (option === 'month') {
        await getCaloriesByMonthAction(cancelToken);
      }
      else {
        await getCaloriesByRangeAction(cancelToken);
      }
    });
  };

  const setGraph = () => {
    if (option === 'month') {
      data = {
        labels: dietMonth.map((item) => item.day),
        datasets: [
          {
            label: 'Calories',
            data: dietMonth.map((item) => parseFloat(item.calories.toFixed(2))),
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            borderColor: 'rgb(255, 99, 132)'
          }
        ]
      };
    }
    else {
      data = {
        labels: dietRange.map((item) => item.day),
        datasets: [
          {
            label: 'Calories',
            data: dietRange.map((item) => parseFloat(item.calories.toFixed(2))),
            backgroundColor: 'rgba(255, 99, 132, 0.5)',
            borderColor: 'rgb(255, 99, 132)'
          }
        ]
      };
    }
  };

  useEffect(() => {
    getGraphData();
  }, [dateRange, month, option]);

  useEffect(() => {
    if (dietMonth.length !== 0) {
      setGraph();
    }
    if (dietRange.length !== 0) {
      setGraph();
    }
  }, [dietMonth, dietRange]);

  return (
    <Box sx={{ display: 'flex', width: '100%', flexDirection: 'column' }}>
      <CustomizedSnackbar {...snackbar} handleClose={handleCloseSnackbar} />
      {isLoading ? (
        <CustomizedProgress position="center" />
      ) : (
        <>
          {option === 'month' && dietMonth.length !== 0 && <Line data={data} options={options} />}
          {option === 'range' && (dietRange.length !== 0 ? (<Line data={data} options={options} />) : (<p style={{ textAlign: 'center' }}>No data to display</p>))}
        </>
      )}
    </Box>
  );
}