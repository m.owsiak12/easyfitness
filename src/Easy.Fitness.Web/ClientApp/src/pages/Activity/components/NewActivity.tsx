import { useState } from 'react';
import { ActivityDto, DurationInterface } from '../../../api/types/activity';
import CustomizedSnackbar, { SnackbarInterface } from '../../../components/CustomizedSnackbar';
import { useCancellationToken } from '../../../hooks/useCancellationToken';
import { addNewActivity } from '../../../api/activity';
import { isCancel, Error } from '../../../api/axiosSource';
import { Backdrop, Box, Button, Divider, Fade, Grid, IconButton, InputLabel, MenuItem, Modal, OutlinedInput, Select, SelectChangeEvent, Typography } from '@mui/material';
import styles from './modules/newActivity.module.css';
import { StyledTooltip } from '../../../components/StyledTooltip';
import CustomizedProgress from '../../../components/CustomizedProgress';
import CloseIcon from '@mui/icons-material/Close';
import LocalFireDepartmentIcon from '@mui/icons-material/LocalFireDepartment';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import DurationPicker from 'react-duration-picker';

interface NewActivityProps {
  open: boolean;
  onClose: () => void;
}

export default function NewActivity({ open, onClose }: NewActivityProps) {
  const [type, setType] = useState<string>('');
  const [duration, setDuration] = useState<DurationInterface>({ hours: 0, minutes: 0, seconds: 0 });
  const [newActivity, setNewActivity] = useState<ActivityDto>({ date: '', type: '', name: '0', calories: 0, duration: '' });
  const [snackbar, setSnackbar] = useState<SnackbarInterface>({ open: false, type: undefined, message: '' });
  const [isSubmittingActivity, setIsSubmittingActivity] = useState<boolean>(false);

  const cancellation = useCancellationToken();

  const handleTypeChange = (e: SelectChangeEvent) => {
    setType(e.target.value as string);
    handleActivityTypeChange(e.target.value as string);
  };

  const handleDurationChange = (durationInput: any) => {
    setDuration(durationInput);
    let durationToString = duration.hours + ":" + duration.minutes + ":" + duration.seconds;
    setNewActivity(prev => ({
      ...prev,
      duration: durationToString
    }));
  };

  const handleDateChange = (e: any) => {
    setNewActivity(prev => ({
      ...prev,
      date: e.target.value
    }));
  };

  const handleActivityTypeChange = (type: string) => {
    setNewActivity(prev => ({
      ...prev,
      type: type
    }));
  };

  const handleNameChange = (e: any) => {
    setNewActivity(prev => ({
      ...prev,
      name: e.target.value
    }));
  };

  const handleCaloriesChange = (e: any) => {
    setNewActivity(prev => ({
      ...prev,
      calories: e.target.value as number
    }));
  };

  const handleCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      open: false
    }));
  };

  const addNewActivityAction = async (cancelToken: any) => {
    setIsSubmittingActivity(true);
    return addNewActivity(
      newActivity,
      cancelToken
    )
      .then(() => {
        setSnackbar({
          open: true,
          type: "success",
          message: "New activity has been saved successfully"
        });
        setIsSubmittingActivity(false);
        setTimeout(() => {
          setSnackbar(prev => ({
            ...prev,
            open: false
          }));
          onClose();
        }, 2000);
      })
      .catch((e: Error) => {
        if (!isCancel(e)) {
          setSnackbar({
            open: true,
            type: "error",
            message: e.response.data
          });
        }
        setIsSubmittingActivity(false);
      });
  };

  const onSaveNewActivityClick = async () => {
    cancellation((cancelToken) => {
      addNewActivityAction(cancelToken);
    });
  };

  if (!open) {
    return null;
  }
  return (
    <Box>
      <CustomizedSnackbar {...snackbar} handleClose={handleCloseSnackbar} />
      <Modal
        open={open}
        onClose={onClose}
        closeAfterTransition
        slots={{ backdrop: Backdrop }}
        slotProps={{
          backdrop: {
            timeout: 500,
          },
        }}>
        <Fade in={open}>
          <Box className={styles.newActivityContainer}>
            <Typography sx={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
              <p id={styles.newActivityHeaderText}>Add new activity</p>
              <StyledTooltip title={"Close"}>
                <IconButton
                  size="medium"
                  onClick={onClose}
                >
                  <CloseIcon color="error" />
                </IconButton>
              </StyledTooltip>
            </Typography>
            <Divider sx={{ borderBottomWidth: 2 }} />
            <Box className={styles.newActivityForm}>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <InputLabel>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <CalendarMonthIcon />
                      <span style={{ fontFamily: 'Lexend' }}>Date</span>
                    </Box>
                  </InputLabel>
                  <OutlinedInput
                    required
                    type='date'
                    className={styles.newActivityInput}
                    onChange={handleDateChange}
                  />
                </Grid>
                <Grid item xs={12} sm={6}>
                  <InputLabel>
                    <span style={{ fontFamily: 'Lexend' }}>Type</span>
                  </InputLabel>
                  <Select
                    fullWidth
                    value={type}
                    required
                    onChange={handleTypeChange}
                    style={{
                      fontFamily: 'Lexend'
                    }}
                  >
                    <MenuItem className={styles.selectOptions} value={'Gym'}>Gym</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Swimming'}>Swimming</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Running'}>Running</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Cycling'}>Cycling</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Trekking'}>Trekking</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Walking'}>Walking</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Football'}>Football</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Volleyball'}>Volleyball</MenuItem>
                    <MenuItem className={styles.selectOptions} value={'Other'}>Other</MenuItem>
                  </Select>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <InputLabel>
                    <span style={{ fontFamily: 'Lexend' }}>Name</span>
                  </InputLabel>
                  <OutlinedInput
                    required
                    fullWidth
                    placeholder='New activity'
                    className={styles.newActivityInput}
                    onChange={handleNameChange}
                  />
                </Grid>
                <Grid item xs={12} sm={12}>
                  <InputLabel>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <LocalFireDepartmentIcon color="error" />
                      <span style={{ fontFamily: 'Lexend' }}>Calories</span>
                    </Box>
                  </InputLabel>
                  <Box sx={{ display: 'flex', alignItems: 'center', width: "100%" }}>
                    <OutlinedInput
                      required
                      placeholder='500'
                      className={styles.newActivityInput}
                      type='number'
                      fullWidth
                      onChange={handleCaloriesChange}
                    />
                    <span style={{ fontFamily: 'Lexend', marginLeft: '1ch' }}>cal</span>
                  </Box>
                </Grid>
                <Grid item xs={12} sm={12}>
                  <InputLabel>
                    <Box sx={{ display: 'flex', alignItems: 'center' }}>
                      <AccessTimeIcon />
                      <span style={{ fontFamily: 'Lexend' }}>Duration</span>
                    </Box>
                  </InputLabel>
                  <DurationPicker
                    onChange={handleDurationChange}
                    initialDuration={{ hours: 1, minutes: 30, seconds: 0 }}
                    maxHours={9}
                  />
                </Grid>
              </Grid>
              {!isSubmittingActivity ? (
                <Button id={styles.saveButton} onClick={onSaveNewActivityClick}>Save</Button>
              ) : (
                <CustomizedProgress position='flex-end' />
              )}
            </Box>
          </Box>
        </Fade>
      </Modal>
    </Box>
  );
}