import { CircularProgress, Box } from '@mui/material';

interface ProgressInterface {
  position: string;
}

export default function CustomizedProgress({ position }: ProgressInterface) {
  return (
    <Box sx={{ display: 'flex', alignSelf: position, mt: '1ch' }}>
      <CircularProgress color={'info'} size={30} />
    </Box>
  );
}