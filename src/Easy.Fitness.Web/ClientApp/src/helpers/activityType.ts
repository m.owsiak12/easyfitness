import CyclingIcon from '../img/activity/cycling.svg';
import GymIcon from '../img/activity/gym.svg';
import RunningIcon from '../img/activity/running.svg';
import SwimmingIcon from '../img/activity/swimming.svg';
import TrekkingIcon from '../img/activity/trekking.svg';
import WalkingIcon from '../img/activity/walk.svg';
import FootballIcon from '../img/activity/football.svg';
import VolleyballIcon from '../img/activity/volleyball.svg';
import OtherIcon from '../img/activity/other.svg';
import NullIcon from '../img/activity/null.svg';

export const getIconFromType = (type: string | null) => {
  if(type === 'Gym') return GymIcon;
  if(type === 'Swimming') return SwimmingIcon;
  if(type === 'Running') return RunningIcon;
  if(type === 'Cycling') return CyclingIcon;
  if(type === 'Trekking') return TrekkingIcon;
  if(type === 'Walking') return WalkingIcon;
  if(type === 'Football') return FootballIcon;
  if(type === 'Volleyball') return VolleyballIcon;
  if(type === 'Other') return OtherIcon;
  if(type === null) return NullIcon;
};