import { Dayjs } from "dayjs";

export const formatDate = (value: number) => {
  return value < 10 ? `0${value}` : `${value}`;
};

export const formatMonth = (value: Dayjs | null) => {
  const month = value!.month() + 1;
  return month < 10 ? `0${month}` : `${month}`;
};