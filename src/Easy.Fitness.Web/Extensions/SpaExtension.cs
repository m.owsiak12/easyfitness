﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace Easy.Fitness.Web.Extensions
{
    public static class SpaExtension
    {
        public static void AddSpa(this IServiceCollection services)
        {
            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        public static void UseSpaUI(this IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.MapWhen((context) => !context.IsApiRequest(), builder => UseSpaWithAuth(env, builder));
        }

        private static void UseSpaWithAuth(IWebHostEnvironment env, IApplicationBuilder builder)
        {
            builder.UseSpaStaticFiles();
            builder.UseSpa(spa =>
            {
                spa.ApplicationBuilder.Use(async (context, next) =>
                {
                    if (!context.User.Identity.IsAuthenticated)
                    {
                        await context.ChallengeAsync();
                    }
                    else
                    {
                        await next();
                    }
                });
                spa.Options.SourcePath = "ClientApp";
#if DEBUG
                if (env.IsDevelopment())
                {
                    spa.Options.StartupTimeout = TimeSpan.FromMinutes(5);
                    spa.UseReactDevelopmentServer(npmScript: "start");
                    // or
                    // proxy to react app running by npm
                    //spa.UseProxyToSpaDevelopmentServer("http://localhost:3000");
                }
#endif
            });
        }
    }
}
