using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.IO;
using System.Reflection;

namespace Easy.Fitness.Web
{
    internal class Program
    {
        private static Serilog.ILogger _log;

        public static void Main(string[] args)
        {
            try
            {
                IConfigurationRoot config = ConfigurationHelper.CreateRoot();
                ConfigureMainLogger(config);
                PrintAndLogVersion();
                var appConfig = ConfigurationHelper.CreateFrom<AppConfiguration>(config);
                string url = appConfig.HostConfiguration.Host;
                IHost host = Host.CreateDefaultBuilder(args)
                    .UseContentRoot(Directory.GetCurrentDirectory())
                    .ConfigureLogging(builder => builder.AddSerilog())
                    .ConfigureWebHostDefaults(builder =>
                    {
                        builder
                            .UseKestrel(o => o.AddServerHeader = false)
                            .UseContentRoot(Directory.GetCurrentDirectory())
                            .UseStartup<Startup>()
                            .UseUrls(url);
                    })
                    .Build();
                MigrateDatabase(host);
                host.Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Problem with application running!");
                Log.CloseAndFlush();
                throw;
            }
        }

        private static void ConfigureMainLogger(IConfigurationRoot config)
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();
            _log = Log.ForContext<Program>();
        }

        private static void PrintAndLogVersion()
        {
            string version = GetVersion();
            _log.Information("Component {version}", version);
            Console.WriteLine($"Version: {version}");
        }

        private static void MigrateDatabase(IHost host)
        {
            using AsyncServiceScope scope = host.Services.CreateAsyncScope();
            AppDbContext dbContext = scope.ServiceProvider.GetRequiredService<AppDbContext>();
            dbContext.Database.Migrate();
        }

        internal static string GetVersion()
        {
            return typeof(Program)
                .GetTypeInfo()
                .Assembly
                .GetName()
                .Version
                .ToString();
        }
    }
}