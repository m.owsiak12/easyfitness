﻿using System.Linq;
using System.Threading.Tasks;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Converters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;

namespace Easy.Fitness.Web.HealthChecks
{
    internal static class HealthChecksHelper
    {
        internal static void RegisterHealthChecks(this IServiceCollection services, AppConfiguration configuration)
        {
            services.AddHealthChecks()
                .AddDbContextCheck<AppDbContext>(
                name: "postgreSQL-check",
                tags: new[] { "db", "ready" });
        }

        internal static void UseMyHealthChecks(this IApplicationBuilder app)
        {
            app.UseHealthChecks("/readyz", new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions
            {
                Predicate = (check) => check.Tags.Contains("ready"),
                ResponseWriter = WriteResponse
            });
            app.UseHealthChecks("/healthz", new Microsoft.AspNetCore.Diagnostics.HealthChecks.HealthCheckOptions
            {
                Predicate = (_) => false,
                ResponseWriter = WriteResponse
            });
        }

        private static Task WriteResponse(HttpContext httpContext, HealthReport result)
        {
            httpContext.Response.ContentType = "application/json";
            var dto = new HealthReportDto
            {
                Status = result.Status,
                Results = result.Entries.Select(pair => new EntryDto
                {
                    Name = pair.Key,
                    Status = pair.Value.Status,
                    Description = pair.Value.Description
                }).ToList()
            };
            string json = JsonConvert.SerializeObject(dto, Formatting.Indented, ConvertersExtensions.GetConverters());
            return httpContext.Response.WriteAsync(json);
        }
    }
}