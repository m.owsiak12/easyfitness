﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Easy.Fitness.Web.HealthChecks
{
    public class EntryDto
    {
        public string Name { get; set; }
        public HealthStatus Status { get; set; }
        public string Description { get; set; }
    }
}