﻿using System.Collections.Generic;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace Easy.Fitness.Web.HealthChecks
{
    public class HealthReportDto
    {
        public HealthStatus Status { get; set; }
        public List<EntryDto> Results { get; set; }
    }
}