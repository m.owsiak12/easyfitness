﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;

namespace Easy.Fitness.Web.Controllers.v1
{
    [ApiVersionNeutral]
    [Authorize]
    public class InfoController : Controller
    {
        /// <summary>
        /// Retrieves info about API
        /// </summary>
        [HttpGet("api/info")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GetInfo()
        {
            string version = Program.GetVersion();
            string[] supportedApiVersions = GetSupportedApiVersions();
            return Json(new
            {
                version,
                supportedApiVersions
            });
        }

        private string[] GetSupportedApiVersions()
        {
            List<Type> controllerTypes = GetType()
                .GetTypeInfo()
                .Assembly
                .GetTypes()
                .Where(x => typeof(Controller).IsAssignableFrom(x))
                .ToList();
            List<string> versions = controllerTypes.SelectMany(t => t.GetTypeInfo()
                        .GetCustomAttributes<ApiVersionAttribute>()
                        .SelectMany(x => x.Versions)
                        .Select(x => x.ToString()))
                     .ToList();
            return versions.Distinct().ToArray();
        }
    }
}