﻿using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Exceptions;
using Easy.Fitness.Application.Interfaces;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Easy.Fitness.Web.Controllers.v1
{
    [ApiVersion("1")]
    [Authorize]
    [Route("api/v{version:apiVersion}")]
    public class ActivityController : Controller
    {
        private readonly IActivityService _activityService;

        public ActivityController(IActivityService activityService)
        {
            _activityService = activityService ?? throw new ArgumentNullException(nameof(activityService));
        }

        /// <summary>
        /// Saves new activity
        /// </summary>
        /// <param name="activity"></param>
        /// <param name="cancellationToken"></param>
        [HttpPost("activity")]
        [ProducesResponseType(typeof(ActivityDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> SaveNewUserActivity([FromBody] ActivityDto activity, CancellationToken cancellationToken)
        {
            try
            {
                ActivityDto result = await _activityService.SaveNewActivityAsync(activity, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Retrieves page of activities
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="cancellationToken"></param>
        [HttpGet("activity")]
        [ProducesResponseType(typeof(PageDto<ActivityDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetActivitiesPage([FromQuery] GetPageCriteria criteria,
            CancellationToken cancellationToken)
        {
            try
            {
                PageDto<ActivityDto> result = await _activityService.GetActivityPageAsync(criteria, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes activity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        [HttpDelete("activity/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteActivity([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            try
            {
                await _activityService.DeleteActivityAsync(id, cancellationToken);
                return Ok();
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates activity
        /// </summary>
        /// <param name="id"></param>
        /// <param name="activity"></param>
        /// <param name="cancellationToken"></param>
        [HttpPut("activity/{id}")]
        [ProducesResponseType(typeof(ActivityDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UpdateActivity(
            [FromRoute] Guid id,
            [FromBody] ActivityDto activity,
            CancellationToken cancellationToken)
        {
            try
            {
                ActivityDto result = await _activityService.UpdateActivityAsync(id, activity, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }
    }
}