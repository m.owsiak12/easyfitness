﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos.Diet;
using Easy.Fitness.Application.Exceptions;
using Easy.Fitness.Application.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Easy.Fitness.Web.Controllers.v1
{
    [ApiVersion("1")]
    [Authorize]
    [Route("api/v{version:apiVersion}")]
    public class DietController : Controller
    {
        private readonly IDietService _dietService;

        public DietController(IDietService dietService)
        {
            _dietService = dietService ?? throw new ArgumentNullException(nameof(dietService));
        }

        /// <summary>
        /// Saves or updates diet properties
        /// </summary>
        /// <param name="dietProperties"></param>
        /// <param name="cancellationToken"></param>
        [HttpPost("diet/properties")]
        [ProducesResponseType(typeof(DietPropertiesDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> SaveDietProperties([FromBody] DietPropertiesDto dietProperties, CancellationToken cancellationToken)
        {
            try
            {
                DietPropertiesDto result = await _dietService.SaveDietPropertiesAsync(dietProperties, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Retrieves diet properties for given date
        /// </summary>
        /// <param name="date"></param>
        /// <param name="cancellationToken"></param>
        [HttpGet("diet/properties/{date}")]
        [ProducesResponseType(typeof(DietPropertiesDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetDietProperties([FromRoute] string date, CancellationToken cancellationToken)
        {
            try
            {
                DietPropertiesDto result = await _dietService.GetDietPropertiesAsync(date, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Saves food to diet
        /// </summary>
        /// <param name="newFood"></param>
        /// <param name="cancellationToken"></param>
        [HttpPost("diet/food")]
        [ProducesResponseType(typeof(FoodDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> AddFoodToDiet([FromBody] AddNewFoodDto newFood, CancellationToken cancellationToken)
        {
            try
            {
                FoodDto result = await _dietService.AddNewFoodToDietAsync(newFood, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ResponseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Retireves list of food names for given foodName
        /// </summary>
        /// <param name="foodName"></param>
        [HttpGet("diet/food")]
        [ProducesResponseType(typeof(List<string>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public IActionResult GetAutocompleteFoodNameList([FromQuery] string foodName)
        {
            try
            {
                List<string> result = _dietService.GetFoodNameList(foodName);
                return Ok(result);
            }
            catch (ResponseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Updates food added to diet
        /// </summary>
        /// <param name="id"></param>
        /// <param name="food"></param>
        /// <param name="cancellationToken"></param>
        [HttpPut("diet/food/{id}")]
        [ProducesResponseType(typeof(FoodDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UpdateFood(
            [FromRoute] Guid id,
            [FromBody] UpdateFoodDto food,
            CancellationToken cancellationToken)
        {
            try
            {
                FoodDto result = await _dietService.UpdateFoodAsync(id, food, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Deletes food
        /// </summary>
        /// <param name="id"></param>
        /// <param name="date"></param>
        /// <param name="cancellationToken"></param>
        [HttpDelete("diet/food/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteFood([FromRoute] Guid id, [FromQuery] string date, CancellationToken cancellationToken)
        {
            try
            {
                await _dietService.DeleteFoodAsync(id, date, cancellationToken);
                return Ok();
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Retrieves diet by given date
        /// </summary>
        /// <param name="date"></param>
        /// <param name="cancellationToken"></param>
        [HttpGet("diet/{date}")]
        [ProducesResponseType(typeof(DietDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetDietByDate([FromRoute] string date, CancellationToken cancellationToken)
        {
            try
            {
                DietDto result = await _dietService.GetDietByDateAsync(date, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Retrieves diet summary for given date
        /// </summary>
        /// <param name="date"></param>
        /// <param name="cancellationToken"></param>
        [HttpGet("diet/{date}/summary")]
        [ProducesResponseType(typeof(DietSummary), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetDietSummaryByDate([FromRoute] string date, CancellationToken cancellationToken)
        {
            try
            {
                DietSummary result = await _dietService.GetDietSummaryByDateAsync(date, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }
    }
}