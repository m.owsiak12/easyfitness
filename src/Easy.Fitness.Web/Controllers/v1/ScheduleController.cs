﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Dtos.Schedule;
using Easy.Fitness.Application.Exceptions;
using Easy.Fitness.Application.Interfaces;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Easy.Fitness.Web.Controllers.v1
{
    [ApiVersion("1")]
    [Authorize]
    [Route("api/v{version:apiVersion}")]
    public class ScheduleController : Controller
    {
        private readonly IScheduleService _scheduleService;

        public ScheduleController(IScheduleService scheduleService)
        {
            _scheduleService = scheduleService ?? throw new ArgumentNullException(nameof(scheduleService));
        }

        /// <summary>
        /// Saves new schedule
        /// </summary>
        /// <param name="schedule"></param>
        /// <param name="cancellationToken"></param>
        [HttpPost("schedule")]
        [ProducesResponseType(typeof(ScheduleDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> SaveNewSchedule([FromBody] ScheduleDto schedule, CancellationToken cancellationToken)
        {
            try
            {
                ScheduleDto result = await _scheduleService.SaveNewScheduleAsync(schedule, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }

        /// <summary>
        /// Retrieves page of schedules
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="cancellationToken"></param>
        [HttpGet("schedule")]
        [ProducesResponseType(typeof(PageDto<ScheduleDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetSchedulePage([FromQuery] GetPageCriteria criteria, CancellationToken cancellationToken)
        {
            try
            {
                PageDto<ScheduleDto> result = await _scheduleService.GetSchedulePageAsync(criteria, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes schedule
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        [HttpDelete("schedule/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteSchedule([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            try
            {
                await _scheduleService.DeleteScheduleAsync(id, cancellationToken);
                return Ok();
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates schedule
        /// </summary>
        /// <param name="id"></param>
        /// <param name="schedule"></param>
        /// <param name="cancellationToken"></param>
        [HttpPut("schedule/{id}")]
        [ProducesResponseType(typeof(ScheduleDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UpdateSchedule([FromRoute] Guid id, [FromBody] ScheduleDto schedule, CancellationToken cancellationToken)
        {
            try
            {
                ScheduleDto result = await _scheduleService.UpdateScheduleAsync(id, schedule, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
        }
    }
}