﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.User;
using Easy.Fitness.Application.Exceptions;
using Easy.Fitness.Application.Interfaces;
using Easy.Fitness.Domain.Models;
using Easy.Fitness.Infrastructure.Authorization;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Easy.Fitness.Web.Controllers.v1
{
    [ApiVersion("1")]
    [Authorize]
    [Route("api/v{version:apiVersion}")]
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService ?? throw new ArgumentNullException(nameof(userService));
        }

        /// <summary>
        /// Creates new user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        [AllowAnonymous]
        [HttpPost("register")]
        [ProducesResponseType(typeof(UserDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> CreateUser([FromBody] CreateUserDto user, CancellationToken cancellationToken)
        {
            try
            {
                UserDto result = await _userService.CreateNewUserAsync(user, cancellationToken);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (UserExistsException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Authenticates user
        /// </summary>
        /// <param name="user"></param>
        /// <param name="cancellationToken"></param>
        [AllowAnonymous]
        [HttpPost("login")]
        [ProducesResponseType(typeof(AccessTokenDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AuthenticateUser([FromBody] CreateUserDto user, CancellationToken cancellationToken)
        {
            try
            {
                string accessToken = await _userService.AuthenticateUserAsync(user, cancellationToken);
                return Ok(new AccessTokenDto
                {
                    AccessToken = accessToken
                });
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidCredentialsException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates user
        /// </summary>
        /// <param name="userData"></param>
        /// <param name="cancellationToken"></param>
        [HttpPut("user")]
        [ProducesResponseType(typeof(UserInfoDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> UpdateUser([FromBody] UserInfoDto userData, CancellationToken cancellationToken)
        {
            try
            {
                UserInfoDto updatedUser = await _userService.UpdateUserAsync(userData, cancellationToken);
                return Ok(updatedUser);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Changes user password
        /// </summary>
        /// <param name="passwordDto"></param>
        /// <param name="cancellationToken"></param>
        [HttpPut("user/password")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDto passwordDto, CancellationToken cancellationToken)
        {
            try
            {
                await _userService.ChangeUserPasswordAsync(passwordDto, cancellationToken);
                return Ok();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (InvalidCredentialsException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates user parameters
        /// </summary>
        /// <param name="userParameters"></param>
        /// <param name="cancellationToken"></param>
        [HttpPost("user/parameters")]
        [ProducesResponseType(typeof(UserParametersDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> SaveUserParameters([FromBody] UserParametersDto userParameters, CancellationToken cancellationToken)
        {
            try
            {
                UserParametersDto result = await _userService.UpdateUserParametersAsync(userParameters, cancellationToken);
                return Ok(result);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves user's info
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet("user")]
        [ProducesResponseType(typeof(UserInfoDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUserInfo(CancellationToken cancellationToken)
        {
            try
            {
                UserInfoDto result = await _userService.GetUserInfoAsync(cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves user's parameters
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("user/parameters")]
        [ProducesResponseType(typeof(UserParametersDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUserParameters(CancellationToken cancellationToken)
        {
            try
            {
                UserParametersDto result = await _userService.GetLatestUserParametersAsync(cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves user's personal info
        /// </summary>
        /// <param name="cancellationToken"></param>
        [HttpGet("user/account")]
        [ProducesResponseType(typeof(UserAccountDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUserPersonalInfo(CancellationToken cancellationToken)
        {
            try
            {
                UserAccountDto result = await _userService.GetUserPersonalInfoAsync(cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves user's summary
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("user/summary")]
        [ProducesResponseType(typeof(UserSummary), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUserSummary(CancellationToken cancellationToken)
        {
            try
            {
                UserSummary result = await _userService.GetUserSummaryAsync(cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Updates user's profile picture
        /// </summary>
        /// <param name="image"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpPut("user/image")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> ChangeUserProfilePicture([FromBody] IFormFile image, CancellationToken cancellationToken)
        {
            try
            {
                await _userService.ChangeUserImageAsync(image, cancellationToken);
                return Ok();
            }
            catch (StorageException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves user's profile picture
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("user/image")]
        [ProducesResponseType(typeof(UserImageDto), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetUserImage(CancellationToken cancellationToken)
        {
            try
            {
                UserImageDto result = await _userService.GetUserImageAsync(cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (StorageException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Deletes user's profile picture
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpDelete("user/image")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteUserImage(CancellationToken cancellationToken)
        {
            try
            {
                await _userService.DeleteUserImageAsync(cancellationToken);
                return Ok();
            }
            catch (StorageException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (NoUserFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}