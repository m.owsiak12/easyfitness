﻿using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Analysis.Activity;
using Easy.Fitness.Application.Dtos.Analysis.Diet;
using Easy.Fitness.Application.Dtos.Analysis.Weight;
using Easy.Fitness.Application.Dtos.Criteria;
using Easy.Fitness.Application.Interfaces;
using Easy.Fitness.Infrastructure.Exceptions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Easy.Fitness.Web.Controllers.v1
{
    [ApiVersion("1")]
    [Authorize]
    [Route("api/v{version:apiVersion}")]
    public class AnalysisController : Controller
    {
        private readonly IAnalysisService _analysisService;

        public AnalysisController(IAnalysisService analysisService)
        {
            _analysisService = analysisService ?? throw new ArgumentNullException(nameof(analysisService));
        }

        /// <summary>
        /// Retrieves data about burned calories by months in specific year
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/activity/month/{month}/year/{year}")]
        [ProducesResponseType(typeof(IEnumerable<ActivityMonthDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetBurnedCaloriesByMonth([FromRoute] string month, string year, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<ActivityMonthDto> result = await _analysisService.GetActivityCaloriesByMonthAsync(month, year, cancellationToken);
                return Ok(result);
            }
            catch(DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves data about burned calories by years
        /// </summary>
        /// <param name="year"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/activity/year/{year}")]
        [ProducesResponseType(typeof(IEnumerable<ActivityYearDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetBurnedCaloriesByYear([FromRoute] string year, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<ActivityYearDto> result = await _analysisService.GetActivityCaloriesByYearAsync(year, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves data about burned calories by specified range
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/activity")]
        [ProducesResponseType(typeof(IEnumerable<ActivityMonthDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetBurnedCaloriesByRange([FromQuery] GetGraphCriteria criteria, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<ActivityMonthDto> result = await _analysisService.GetActivityCaloriesByRangeAsync(criteria, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves data about weigth by specified range
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/weight")]
        [ProducesResponseType(typeof(IEnumerable<WeightMonthDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetWeightByRange([FromQuery] GetGraphCriteria criteria, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<WeightMonthDto> result = await _analysisService.GetWeightByRangeAsync(criteria, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves data about weigth by months in specific year
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/weight/month/{month}/year/{year}")]
        [ProducesResponseType(typeof(IEnumerable<WeightMonthDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetWeightByMonth([FromRoute] string month, string year, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<WeightMonthDto> result = await _analysisService.GetWeightByMonthAsync(month, year, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves data about calories by months in specific year
        /// </summary>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/diet/month/{month}/year/{year}")]
        [ProducesResponseType(typeof(IEnumerable<DietMonthDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetCaloriesByMonth([FromRoute] string month, string year, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<DietMonthDto> result = await _analysisService.GetCaloriesByMonthAsync(month, year, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Retrieves data about calories by specified range
        /// </summary>
        /// <param name="criteria"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet("analysis/diet")]
        [ProducesResponseType(typeof(IEnumerable<DietMonthDto>), 200)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetCaloriesByRange([FromQuery] GetGraphCriteria criteria, CancellationToken cancellationToken)
        {
            try
            {
                IEnumerable<DietMonthDto> result = await _analysisService.GetCaloriesByRangeAsync(criteria, cancellationToken);
                return Ok(result);
            }
            catch (DatabaseException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
