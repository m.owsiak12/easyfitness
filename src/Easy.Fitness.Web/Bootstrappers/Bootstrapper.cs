﻿using Easy.Fitness.Application;
using Easy.Fitness.Application.Interfaces;
using Easy.Fitness.Application.Services;
using Easy.Fitness.Domain.Interfaces;
using Easy.Fitness.Infrastructure;
using Easy.Fitness.Infrastructure.Authorization;
using Easy.Fitness.Infrastructure.Configuration;
using Easy.Fitness.Infrastructure.Repositories;
using Easy.Fitness.Infrastructure.Storage;
using Easy.Fitness.Infrastructure.WebClients.Diet;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Easy.Fitness.Web.Bootstrappers
{
    internal static class Bootstrapper
    {
        internal static void RegisterDependencies(this IServiceCollection services, AppConfiguration configuration)
        {
            SetConfiguration(services, configuration);
            RegisterServices(services);
            RegisterDatabase(services, configuration);
        }

        private static void RegisterServices(IServiceCollection services)
        {
            services.AddScoped<IUserContext, UserContext>();
            services.AddScoped<IUserTokenProvider, UserTokenProvider>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IActivityRepository, ActivityRepository>();
            services.AddScoped<IActivityService, ActivityService>();
            services.AddScoped<IScheduleService, ScheduleService>();
            services.AddScoped<IScheduleRepository, ScheduleRepository>();
            services.AddScoped<IDietService, DietService>();
            services.AddScoped<IDietRepository, DietRepository>();
            services.AddScoped<ISummaryRepository, SummaryRepository>();
            services.AddScoped<ISummaryService, SummaryService>();
            services.AddScoped<IAnalysisRepository, AnalysisRepository>();
            services.AddScoped<IAnalysisService, AnalysisService>();
            services.AddScoped<IFoodDataProvider, FoodDataProvider>();
            services.AddScoped<IFileService, FileService>();
        }

        private static void SetConfiguration(IServiceCollection services, AppConfiguration configuration)
        {
            services.AddSingleton(configuration);
            services.AddSingleton(configuration.HostConfiguration);
            services.AddSingleton(configuration.AuthTokenValidation);
            services.AddSingleton(configuration.FoodDatabaseApi);
        }

        private static void RegisterDatabase(IServiceCollection services, AppConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(
                options => options.UseNpgsql(configuration.PostgresDataBase.ConnectionString), ServiceLifetime.Transient);
        }
    }
}