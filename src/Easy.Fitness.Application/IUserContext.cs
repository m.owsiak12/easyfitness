﻿using Easy.Fitness.Domain.Ids;

namespace Easy.Fitness.Application
{
    public interface IUserContext
    {
        UserId CurrentUserId { get; }
    }
}