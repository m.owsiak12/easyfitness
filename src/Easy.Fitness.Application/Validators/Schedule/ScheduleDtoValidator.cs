﻿using System.Collections.Generic;
using Easy.Fitness.Application.Dtos.Schedule;
using FluentValidation;
using FluentValidation.Results;

namespace Easy.Fitness.Application.Validators.Schedule
{
    public class ScheduleDtoValidator : AbstractValidator<ScheduleDto>
    {
        public ScheduleDtoValidator()
        {
            RuleFor(s => s.Date)
                .NotEmpty().WithMessage("Date cannot be empty")
                .NotNull().WithMessage("Date is required");
            RuleFor(s => s.Type)
                .NotEmpty().WithMessage("Type cannot be empty")
                .NotNull().WithMessage("Type is required");
        }

        protected override void RaiseValidationException(ValidationContext<ScheduleDto> context, ValidationResult result)
        {
            List<KeyValuePair<string, string>> errors = new();
            foreach (ValidationFailure error in result.Errors)
            {
                errors.Add(new KeyValuePair<string, string>(error.PropertyName, error.ErrorMessage));
            }
            throw new Exceptions.ValidationException(errors);
        }
    }
}