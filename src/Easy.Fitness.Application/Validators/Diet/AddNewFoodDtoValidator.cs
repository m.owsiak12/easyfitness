﻿using System.Collections.Generic;
using Easy.Fitness.Application.Dtos.Diet;
using FluentValidation;
using FluentValidation.Results;

namespace Easy.Fitness.Application.Validators.Diet
{
    public class AddNewFoodDtoValidator : AbstractValidator<AddNewFoodDto>
    {
        public AddNewFoodDtoValidator()
        {
            RuleFor(food => food.Name)
                .NotEmpty().WithMessage("Food name cannot be empty")
                .NotNull().WithMessage("Food name is required");

            RuleFor(food => food.Date)
                .NotEmpty().WithMessage("Food date cannot be empty")
                .NotNull().WithMessage("Food date is required");

            RuleFor(food => food.Type)
                .NotEmpty().WithMessage("Food type cannot be empty")
                .NotNull().WithMessage("Food type is required");

            RuleFor(food => food.Weight)
                .NotEmpty().WithMessage("Food weight cannot be empty")
                .NotNull().WithMessage("Food weight is required");
        }

        protected override void RaiseValidationException(ValidationContext<AddNewFoodDto> context, ValidationResult result)
        {
            List<KeyValuePair<string, string>> errors = new();
            foreach (ValidationFailure error in result.Errors)
            {
                errors.Add(new KeyValuePair<string, string>(error.PropertyName, error.ErrorMessage));
            }
            throw new Exceptions.ValidationException(errors);
        }
    }
}