﻿using System.Collections.Generic;
using Easy.Fitness.Application.Dtos.Diet;
using FluentValidation;
using FluentValidation.Results;

namespace Easy.Fitness.Application.Validators.Diet
{
    public class DietPropertiesDtoValidator : AbstractValidator<DietPropertiesDto>
    {
        public DietPropertiesDtoValidator()
        {
            RuleFor(p => p.Date)
                .NotEmpty().WithMessage("Properties date cannot be empty")
                .NotNull().WithMessage("Properties date is required");

            RuleFor(p => p.Calories)
                .NotEmpty().WithMessage("Properties calories cannot be empty")
                .NotNull().WithMessage("Properties calories are required");

            RuleFor(p => p.Fat)
                .NotEmpty().WithMessage("Properties fat cannot be empty")
                .NotNull().WithMessage("Properties fat is required");

            RuleFor(p => p.Carbs)
                .NotEmpty().WithMessage("Properties carbs cannot be empty")
                .NotNull().WithMessage("Properties carbs are required");

            RuleFor(p => p.Protein)
                .NotEmpty().WithMessage("Properties protein cannot be empty")
                .NotNull().WithMessage("Properties protein is required");
        }

        protected override void RaiseValidationException(ValidationContext<DietPropertiesDto> context, ValidationResult result)
        {
            List<KeyValuePair<string, string>> errors = new();
            foreach (ValidationFailure error in result.Errors)
            {
                errors.Add(new KeyValuePair<string, string>(error.PropertyName, error.ErrorMessage));
            }
            throw new Exceptions.ValidationException(errors);
        }
    }
}