﻿using System.Collections.Generic;
using Easy.Fitness.Application.Dtos.Activity;
using FluentValidation;
using FluentValidation.Results;

namespace Easy.Fitness.Application.Validators.Activity
{
    public class ActivityDtoValidator : AbstractValidator<ActivityDto>
    {
        public ActivityDtoValidator()
        {
            RuleFor(a => a.Date)
                .NotEmpty().WithMessage("Date cannot be empty")
                .NotNull().WithMessage("Date is required");
            RuleFor(a => a.Type)
                .NotEmpty().WithMessage("Type cannot be empty")
                .NotNull().WithMessage("Type is required");
            RuleFor(a => a.Name)
                .NotEmpty().WithMessage("Name cannot be empty")
                .NotNull().WithMessage("Name is required");
            RuleFor(a => a.Calories)
                .NotEmpty().WithMessage("Calories cannot be empty")
                .NotNull().WithMessage("Calories are required");
            RuleFor(a => a.Duration)
                .NotEmpty().WithMessage("Duration cannot be empty")
                .NotNull().WithMessage("Duration is required");
        }

        protected override void RaiseValidationException(ValidationContext<ActivityDto> context, ValidationResult result)
        {
            List<KeyValuePair<string, string>> errors = new();
            foreach (ValidationFailure error in result.Errors)
            {
                errors.Add(new KeyValuePair<string, string>(error.PropertyName, error.ErrorMessage));
            }
            throw new Exceptions.ValidationException(errors);
        }
    }
}