﻿using System.Collections.Generic;
using Easy.Fitness.Domain.Models;

namespace Easy.Fitness.Application
{
    public interface IFoodDataProvider
    {
        FoodDetails GetFoodDetails(string foodName);
        List<string> GetFoodNameHints(string foodName);
    }
}