﻿using Easy.Fitness.Application.Dtos;
using Easy.Fitness.Application.Dtos.Activity;
using Easy.Fitness.Application.Dtos.Diet;
using Easy.Fitness.Application.Dtos.Schedule;
using Easy.Fitness.Application.Dtos.User;
using Easy.Fitness.Application.Validators.Activity;
using Easy.Fitness.Application.Validators.Diet;
using Easy.Fitness.Application.Validators.Schedule;
using Easy.Fitness.Application.Validators.User;
using FluentValidation;

namespace Easy.Fitness.Application.Extensions
{
    public static class ValidatorsExtension
    {
        private readonly static CreateUserDtoValidator _createUserDtoValidator = new();
        private readonly static ChangePasswordDtoValidator _passwordDtoValidator = new();
        private readonly static UserParametersDtoValidator _userParametersDtoValidator = new();
        private readonly static ActivityDtoValidator _activityDtoValidator = new();
        private readonly static ScheduleDtoValidator _scheduleDtoValidator = new();
        private readonly static AddNewFoodDtoValidator _addNewFoodDtoValidator = new();
        private readonly static DietPropertiesDtoValidator _dietPropertiesDtoValidator = new();
        private readonly static UpdateFoodDtoValidator _updateFoodDtoValidator = new();

        public static void Validate(this CreateUserDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _createUserDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this ChangePasswordDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _passwordDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this UserParametersDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _userParametersDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this ActivityDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _activityDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this ScheduleDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _scheduleDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this AddNewFoodDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _addNewFoodDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this DietPropertiesDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _dietPropertiesDtoValidator.ValidateAndThrow(dto);
        }

        public static void Validate(this UpdateFoodDto dto)
        {
            if (dto == null)
            {
                throw new Exceptions.ValidationException();
            }
            _updateFoodDtoValidator.ValidateAndThrow(dto);
        }
    }
}