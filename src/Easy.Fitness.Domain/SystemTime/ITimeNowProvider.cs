﻿using System;

namespace Easy.Fitness.Domain.SystemTime
{
    public interface ITimeNowProvider
    {
        DateTime Now { get; }
    }
}