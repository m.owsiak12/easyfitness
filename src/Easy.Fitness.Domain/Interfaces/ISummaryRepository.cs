﻿using Easy.Fitness.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Easy.Fitness.Domain.Interfaces
{
    public interface ISummaryRepository
    {
        Task<DashboardSummary> GetSummaryAsync(string date, CancellationToken cancellationToken);
    }
}
