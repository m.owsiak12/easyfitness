﻿using Easy.Fitness.Domain.Models;
using System.Threading;
using System.Threading.Tasks;

namespace Easy.Fitness.Domain.Interfaces
{
    public interface IFileService
    {
        Task SaveFileAsync(UserImage file, CancellationToken cancellationToken);
        Task<string> GetFileAsync(string fileName, CancellationToken cancellationToken);
        Task RemoveFileAsync(string fileName, CancellationToken cancellationToken);
    }
}
