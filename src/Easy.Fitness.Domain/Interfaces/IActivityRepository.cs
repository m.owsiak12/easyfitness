﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Easy.Fitness.Domain.Models;

namespace Easy.Fitness.Domain.Interfaces
{
    public interface IActivityRepository
    {
        Task<Activity> SaveNewActivityAsync(Activity activity, CancellationToken cancellationToken);
        Task<IEnumerable<Activity>> GetActivitiesAsync(int page, string sortColumn, bool isDescending, string searchType, string searchDate, CancellationToken cancellationToken);
        Task<int> GetTotalCountAsync(CancellationToken cancellationToken);
        Task DeleteActivityAsync(Guid id, CancellationToken cancellationToken);
        Task<Activity> UpdateActivityAsync(Guid id, Activity activity, CancellationToken cancellationToken);
    }
}