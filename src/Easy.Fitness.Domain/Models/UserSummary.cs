﻿namespace Easy.Fitness.Domain.Models
{
    public class UserSummary
    {
        public int Trainings { get; set; }
        public double Calories { get; set; }
    }
}
