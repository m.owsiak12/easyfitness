﻿using System;
using Easy.Fitness.Domain.Ids;

namespace Easy.Fitness.Domain.Models
{
    public class PlannedActivity : Entity<Guid>
    {
        public string Date { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public Guid UserId { get; private set; }
        public User User { get; private set; }

        public PlannedActivity(string date, string type, string note, UserId createdBy) : base(createdBy)
        {
            Date = !string.IsNullOrWhiteSpace(date) ? date : throw new ArgumentNullException(date);
            Type = !string.IsNullOrWhiteSpace(type) ? type : throw new ArgumentNullException(type);
            Note = note;
        }

        public PlannedActivity(string date, string type, string note)
        {
            Date = !string.IsNullOrWhiteSpace(date) ? date : throw new ArgumentNullException(date);
            Type = !string.IsNullOrWhiteSpace(type) ? type : throw new ArgumentNullException(type);
            Note = note;
        }
    }
}