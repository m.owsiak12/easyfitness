﻿using System;
using Easy.Fitness.Domain.Ids;

namespace Easy.Fitness.Domain
{
    public class UserParameters : Entity<Guid>
    {
        public double Weight { get; set; }
        public double Height { get; set; }
        public Guid UserId { get; private set; }
        public User User { get; private set; }

        public UserParameters(double weight, double height, UserId createdBy) : base(createdBy)
        {
            Weight = weight;
            Height = height;
        }

        public UserParameters(double weight, double height)
        {
            Weight = weight;
            Height = height;
        }
    }
}