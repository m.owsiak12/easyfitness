﻿using System;
using Easy.Fitness.Domain.Ids;

namespace Easy.Fitness.Domain.Models
{
    public class Activity : Entity<Guid>
    {
        public string Date { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }
        public double Calories { get; set; }
        public string Duration { get; set; }
        public Guid UserId { get; private set; }
        public User User { get; private set; }

        public Activity(string date, string type, string name, double calories, string duration, UserId createdBy) : base(createdBy)
        {
            Date = !string.IsNullOrWhiteSpace(date) ? date : throw new ArgumentNullException(date);
            Type = !string.IsNullOrWhiteSpace(type) ? type : throw new ArgumentNullException(type);
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(name);
            Calories = calories;
            Duration = !string.IsNullOrWhiteSpace(duration) ? duration : throw new ArgumentNullException(duration);
        }

        public Activity(string date, string type, string name, double calories, string duration)
        {
            Date = !string.IsNullOrWhiteSpace(date) ? date : throw new ArgumentNullException(date);
            Type = !string.IsNullOrWhiteSpace(type) ? type : throw new ArgumentNullException(type);
            Name = !string.IsNullOrWhiteSpace(name) ? name : throw new ArgumentNullException(name);
            Calories = calories;
            Duration = !string.IsNullOrWhiteSpace(duration) ? duration : throw new ArgumentNullException(duration);
        }
    }
}