﻿using System;
using System.Collections.Generic;
using Easy.Fitness.Domain.Ids;

namespace Easy.Fitness.Domain.Models
{
    public class Diet : Entity<Guid>
    {
        public string Date { get; set; }
        public double Calories { get; set; }
        public double Fat { get; set; }
        public double Carbs { get; set; }
        public double Protein { get; set; }
        public ICollection<Food> Foods { get; set; } = new List<Food>();
        public DietProperties Properties { get; set; }
        public Guid UserId { get; private set; }
        public User User { get; private set; }

        public Diet(string date, double calories, double fat, double carbs, double protein)
        {
            Date = !string.IsNullOrEmpty(date) ? date : throw new ArgumentNullException(nameof(date));
            Calories = calories;
            Fat = fat;
            Carbs = carbs;
            Protein = protein;
        }

        public Diet(string date, double calories, double fat, double carbs, double protein, UserId userId) : base(userId)
        {
            Date = !string.IsNullOrEmpty(date) ? date : throw new ArgumentNullException(nameof(date));
            Calories = calories;
            Fat = fat;
            Carbs = carbs;
            Protein = protein;
        }

    }
}