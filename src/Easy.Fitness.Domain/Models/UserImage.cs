﻿using System;
using System.IO;

namespace Easy.Fitness.Domain.Models
{
    public class UserImage
    {
        public Stream Stream { get; private set; }
        public string FileName { get; private set; }
        public long Size { get; private set; }

        public UserImage(Stream stream, string fileName, long size)
        {
            Stream = stream ?? throw new ArgumentNullException(nameof(stream));
            FileName = !string.IsNullOrWhiteSpace(fileName) ? fileName : throw new ArgumentNullException(nameof(fileName));
            Size = size;
        }

        public UserImage(Stream stream, string fileName)
        {
            Stream = stream ?? throw new ArgumentNullException(nameof(stream));
            FileName = !string.IsNullOrWhiteSpace(fileName) ? fileName : throw new ArgumentNullException(nameof(fileName));
        }
    }
}
