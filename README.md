# EasyFitness
## Table of Contents
* [General Info](#general-information)
* [Technologies Used](#technologies-used)
* [Use Cases](#use-cases)
* [Database Diagram](#database-diagram)
* [Features](#features)
* [Screenshots](#screenshots)
* [Usage](#usage)
* [Contact](#contact)

## General Information
EasyFitness is a web application for managing diet and physical activity with progress indicators in form of charts.

## Technologies Used
- React with TypeScript
- C# with .NET 6
- PostgreSQL
- MinIO
- Docker

## Use Cases
![us1](/uploads/74aa0d3dccfad89c9c845b5d7d758aaa/us1.png)
![us2](/uploads/1cee2b8c2af475bae6d335eee3b0cad4/us2.png)
![us3](/uploads/d6cc9f1611b47d59c478dbd50f9dcfba/us3.png)
![us4](/uploads/301b9b9713c184aef71ecd7b250091c3/us4.png)

## Database Diagram
![erd](/uploads/f26d6642d38a40dc266ea9c3b362bdfb/erd.png)

## Features
- Creating account and login to application
- Update your profile
- Set your everyday diet goal
- Add meals to your diet
- Add done activities or plan a new one
- Look through your activities and schedule
- Keep track of your progress by generating charts

## Screenshots
![dashboard](/uploads/f4494c7945da30be3b429b712ff2f4a0/dashboard.PNG)
![progres](/uploads/5c1073381981d81d95922c2d3b808bd1/progres.PNG)
![activity](/uploads/af2090274464bc489bc21f5b63a4706c/activity.PNG)
![schedule](/uploads/7f32dd577c746c064bc0c0c68b2eb208/schedule.PNG)
![diet](/uploads/865492835de89c8ad32e1e03bcf422a9/diet.PNG)
![account](/uploads/865daa984ec782ddef79517eb464ef84/account.PNG)

## Usage
- Visit https://easyfitness.pl
- Create a new account
- Login to your account and enjoy the application!

## Contact
Created by Mateusz Owsiak - feel free to contact me!